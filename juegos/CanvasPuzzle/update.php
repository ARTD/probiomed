<!DOCTYPE html>
<html lang="es-mx">
<head>
    <meta charset="utf-8" />
    <title>¡ Felicidades !</title>
    <meta name="description" content="Sitio exclusivo para Probiomed-Puntuación Rompecabezas" />
  <link type="image/x-icon" href="images/favicon.ico" rel="icon"/>
  <link rel="stylesheet" href="css/style.css" />
</head>
<body>
    <header id="cabecera">
       <img src="images/logo_pr.png" />
    </header>
    <section class="recuperacion">
         <h2>Rompecabezas</h2>
    <?php
        require ("../../funciones.php");
        include('../../conexion.php');

        $user=$_SESSION['usuarios'];

        // Se toma el tiempo y las piezas del jugador
        $tiempo = $_POST['time'];
        $piezas = $_POST['parts'];

        // Se crean dos variables de tiempo para comparar el tiempo en que termino el jugador
        $oro = date("00:01:00");
        $plata = date("00:01:30");

        if ($piezas == 10) {
            if ($tiempo < $oro) {
                echo "<br/>Ha ganado 10 puntos";
                $puntos = 10;
            }
            elseif ($tiempo < $plata) {
                echo "<br/>Ha ganado 5 puntos";
                $puntos = 5;
            }
            else{
                echo "<br/>Ha ganado 1 puntos";
                $puntos = 1;
            }    
        }
        elseif ($piezas == 20) {
            $oro = date("00:02:00");
            $plata = date("00:02:30");
            if ($tiempo < $oro) {
                echo "<br/>Ha ganado 15 puntos";
                $puntos = 15;
            }
            elseif ($tiempo < $plata) {
                echo "<br/>Ha ganado 10 puntos";
                $puntos = 10;
            }
            else{
                echo "<br/>Ha ganado 3 puntos";
                $puntos = 3;
            }    
        }
        elseif ($piezas == 30) {
            $oro = date("00:04:00");
            $plata = date("00:04:30");
            if ($tiempo < $oro) {
                echo "<br/>Ha ganado 20 puntos";
                $puntos = 20;
            }
            elseif ($tiempo < $plata) {
                echo "<br/>Ha ganado 15 puntos";
                $puntos = 15;
            }
            else{
                echo "<br/>Ha ganado 7 puntos";
                $puntos = 7;
            }    
        }
        elseif ($piezas == 40) {
            $oro = date("00:06:00");
            $plata = date("00:06:30");
            if ($tiempo < $oro) {
                echo "<br/>Ha ganado 25 puntos";
                $puntos = 25;
            }
            elseif ($tiempo < $plata) {
                echo "<br/>Ha ganado 20 puntos";
                $puntos = 5;
            }
            else{
                echo "<br/>Ha ganado 10 puntos";
                $puntos = 1;
            }    
        }
        elseif ($piezas == 50) {
            $oro = date("00:08:00");
            $plata = date("00:08:30");
            if ($tiempo < $oro) {
                echo "<br/>Ha ganado 30 puntos";
                $puntos = 30;
            }
            elseif ($tiempo < $plata) {
                echo "<br/>Ha ganado 25 puntos";
                $puntos = 25;
            }
            else{
                echo "<br/>Ha ganado 15 puntos";
                $puntos = 15;
            }    
        }
        elseif ($piezas == 60) {
            $oro = date("00:10:00");
            $plata = date("00:010:30");
            if ($tiempo < $oro) {
                echo "<br/>Ha ganado 40 puntos";
                $puntos = 40;
            }
            elseif ($tiempo < $plata) {
                echo "<br/>Ha ganado 35 puntos";
                $puntos = 35;
            }
            else{
                echo "<br/>Ha ganado 20 puntos";
                $puntos = 20;
            }    
        }
        elseif ($piezas == 70) {
            $oro = date("00:12:00");
            $plata = date("00:12:30");
            if ($tiempo < $oro) {
                echo "<br/>Ha ganado 50 puntos";
                $puntos = 50;
            }
            elseif ($tiempo < $plata) {
                echo "<br/>Ha ganado 45 puntos";
                $puntos = 45;
            }
            else{
                echo "<br/>Ha ganado 25 puntos";
                $puntos = 25;
            }    
        }
        elseif ($piezas == 80) {
            $oro = date("00:14:00");
            $plata = date("00:14:30");
            if ($tiempo < $oro) {
                echo "<br/>Ha ganado 60 puntos";
                $puntos = 60;
            }
            elseif ($tiempo < $plata) {
                echo "<br/>Ha ganado 50 puntos";
                $puntos = 50;
            }
            else{
                echo "<br/>Ha ganado 30 puntos";
                $puntos = 30;
            }    
        }
        elseif ($piezas == 90) {
            $oro = date("00:16:00");
            $plata = date("00:16:30");
            if ($tiempo < $oro) {
                echo "<br/>Ha ganado 70 puntos";
                $puntos = 70;
            }
            elseif ($tiempo < $plata) {
                echo "<br/>Ha ganado 55 puntos";
                $puntos = 55;
            }
            else{
                echo "<br/>Ha ganado 35 puntos";
                $puntos = 35;
            }    
        }
        elseif ($piezas == 100) {
            $oro = date("00:18:00");
            $plata = date("00:18:30");
            if ($tiempo < $oro) {
                echo "<br/>Ha ganado 80 puntos";
                $puntos = 80;
            }
            elseif ($tiempo < $plata) {
                echo "<br/>Ha ganado 60 puntos";
                $puntos = 60;
            }
            else{
                echo "<br/>Ha ganado 40 puntos";
                $puntos = 40;
            }    
        }

        echo "<br/>Piezas: " . $piezas;    
        echo "<br/>Tiempo de usuario: " . $tiempo;

        // Se hace una insercion a la tabla de juegos para poder hacer un reporte sobre la frecuencia de los juegos
        mysqli_query($con,"UPDATE usuarios SET puntos = puntos + '$puntos' WHERE id_usuarios = '$user'");
        // header("Location: index.php");

        // Se cierra la conexion para no volcar la memoria
        mysqli_close($con);
        ?>
        <aside class="boton">
            <a href="../CanvasPuzzle/index.php">Jugar de Nuevo</a>
            <a href="../../contenido.php">Página de inicio</a>
        </aside>
        </section>
</body>
</html>
