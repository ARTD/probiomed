<?php include_once("include/translate.php"); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <title><?php echo _("Jigsaw Puzzle"); ?></title>
    <link type="image/x-icon" href="images/favicon.ico" rel="icon"/>
    <!--[if lt IE 9]><script type="text/javascript" src="js/bin/flashcanvas.js"></script><![endif]-->
    <link rel="stylesheet" href="css/modal.css" type="text/css" charset="utf-8" />
    <link rel="stylesheet" href="css/style.css" type="text/css" charset="utf-8" />
    <link rel="stylesheet" href="css/buttons.css" type="text/css" charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body>

    <?php
        include('../../conexion.php');

        // Se hace una insercion a la tabla de juegos para poder hacer un reporte sobre la frecuencia de los juegos
        mysqli_query($con,"UPDATE juegos SET veces_jugadas = veces_jugadas + 1 WHERE id_juegos = 4");

        // Se cierra la conexion para no volcar la memoria
        mysqli_close($con);
      ?>

<!-- JIGSAW CANVAS -->
<div id="canvas-wrap">
    <canvas id="canvas"></canvas>
    <canvas class="hide" id="image"></canvas>
    <canvas class="hide" id="image-preview"></canvas>
</div>

<!-- GAME OPTIONS -->
<div id="game-options">
<ul>
    <li><b id="clock" class="button">00:00:00</b></li>
    <li><a href="#" id="JIGSAW_SHUFFLE" class="button left" title="<?php echo _('Shuffle'); ?>"><?php echo _("Shuffle"); ?></a></li>
    <li><a href="#" id="SHOW_PREVIEW" class="button middle" title="<?php echo _('Preview'); ?>"><?php echo _("Preview"); ?></a></li>
    <li><a href="#" id="SHOW_HELP" class="button help right" title="<?php echo _('Help'); ?>"><?php echo _("Help"); ?></a></li>
    <!-- INSERT CUSTOM BUTTONS -->
    
    <!-- END INSERT CUSTOM BUTTONS -->
    <li>
        <div class="styled-select">
            <select id="set-parts" selected-index="0"></select>
        </div>
    </li>
    <!-- Insert custom buttons here -->
    <li id="puntaje">
        <?php
            require ("../../funciones.php");
            include('../../conexion.php');

            $user=$_SESSION['usuarios'];

            // Se selecciona todo el usuario y puntos de la tabla usuarios, donde la información pertenezca al usuario logueado
            $result = mysqli_query($con,"SELECT usuario,puntos FROM usuarios WHERE id_usuarios = '$user'");

            while($row = mysqli_fetch_array($result)) {
                // Se despliega el usuario logueado y sus puntos correspondientes
                echo " Usuario: ".$row['usuario'];
                echo " Puntos: ".$row['puntos'];
            }

            // Se cierra la conexion para no volcar la memoria
            mysqli_close($con);
        ?>
    </li>
    <li id="create"></li>
</ul>
<a id="regresar" href="../../contenido.php"><img src="images/PASTILLA regresar.png"></a>
<br class="clear"/>
</div>

<!-- MODAL WINDOW -->
<div class="hide" id="overlay"></div>
<div id="modal-window" class="hide">
    <div id="modal-window-msg"></div>
    <a href="#" id="modal-window-close" class="button"><?php echo _("Close"); ?></a>
</div>

<!-- CONGRATULATION -->
<div id="congrat" class="hide">
    <h1><?php echo _("Congratulations!"); ?></h1>
    <h2><?php echo _("You solved it in"); ?></h2>
    <h3><span id="time"></span></h3>
    <form method="post" action="update.php" target="save score" onsubmit="jigsaw.UI.close_lightbox();">
        <input type="submit" target="_self" value="<?php echo _('Save score'); ?>" />
        <input type="hidden" id="time-input" name="time"/>
    </form>
</div>

<!-- CREATE PUZZLE -->
<input type="file" id="image-input">
<p id="dnd"></p>

<!-- HELP -->
<div id="help" class="hide">
    <h2><?php echo _("How to play"); ?></h2>
    <ul>
        <li><?php echo _("Change the number of pieces with the selector on the top."); ?><br/>
            <img src="images/selector.png"/>
        </li>
        
        <li><?php echo _("Use left/right arrows, or right click to rotate a piece."); ?></li>

        <!-- <li><?php echo _("Toggle between edge or middle pieces:"); ?><br>
            <img src="images/toggle.png"/>
        </li> -->
        <li>
            Una vez revueltas las piezas, muévelas con el mouse y gíralas con las flechas del teclado.
        </li>
    </ul>
    
    <h3><?php echo _("Good luck."); ?></h3>
</div>

<form class="hide" method="post" id="redirect-form">
    <input type="text" name="time" id="t" />
    <input type="text" name="parts" id="p" />
</form>
<iframe class="hide" src="about:blank" id="save-score" name="save-score"></iframe>
<!-- SCRIPTS ROMPECABEZAS -->
<script src="js/event-emiter.min.js"></script>
<script src="js/canvas-event.min.js"></script>
<script src="js/canvas-puzzle.min.js"></script>
<!--[if lt IE 9]><script type="text/javascript" src="js/canvas-puzzle.ie.min.js"></script><![endif]-->
<script>
;(function() {
    var num = Math.floor((Math.random()*4)+1);
var jsaw = new jigsaw.Jigsaw({
        defaultImage: "images/puzzle/"+num+".jpg",
        maxWidth: 900,
        maxHeight: 400,
        redirect: "update.php",
        piecesNumberTmpl: "<?php echo _('%d Pieces'); ?>"
    });

    if (jigsaw.GET["image"]) { jsaw.set_image(jigsaw.GET["image"]); }
}());
</script>
</body>
</html>
