﻿<!DOCTYPE html>
<html lang="es" xml:lang="en">
	<head>
		<title>Salva al paciente</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link type="image/x-icon" href="img/favicon.ico" rel="icon"/>
		<link rel="stylesheet" href="css/estilos.css" />
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
		<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<! [endif] -->
		<script>
			!window.jQuery && document.write("<script src='js/jquery.min.js'><\/script>");
		</script>
		<script src="js/efectos.js"></script>
	</head>
	<body>
		<?php
			require ("../../funciones.php");
			include('../../conexion.php');

			$user=$_SESSION['usuarios'];
			
	    	$sql = "SELECT usuario,puntos FROM usuarios WHERE id_usuarios = '$user'";
	    	$result = mysqli_query($con,$sql);
			// Se navega en todo el array
			$rows = mysqli_fetch_array($result)
			?>
		<section id="contenedor">
			<article id="contenedorpuntos">
				<p>Usuario: <?php echo $rows['usuario'];?></p>
				<p>Puntos: <?php echo utf8_encode($rows['puntos']);?></p>
			</article>
			<a href="../../contenido.php"><img id="regresar" src="img/PASTILLA regresar.png"></a>
		</section>
		
		<div id="flashContent">
			<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="700" height="400" id="main" align="middle">
				<param name="movie" value="main.swf" />
				<param name="quality" value="high" />
				<param name="bgcolor" value="#000000" />
				<param name="play" value="true" />
				<param name="loop" value="true" />
				<param name="wmode" value="window" />
				<param name="scale" value="showall" />
				<param name="menu" value="true" />
				<param name="devicefont" value="false" />
				<param name="salign" value="" />
				<param name="allowScriptAccess" value="sameDomain" />
				<!--[if !IE]>-->
				<object id="juego" type="application/x-shockwave-flash" data="main.swf" width="700" height="400">
					<param name="movie" value="main.swf" />
					<param name="quality" value="high" />
					<param name="bgcolor" value="#000000" />
					<param name="play" value="true" />
					<param name="loop" value="true" />
					<param name="wmode" value="window" />
					<param name="scale" value="showall" />
					<param name="menu" value="true" />
					<param name="devicefont" value="false" />
					<param name="salign" value="" />
					<param name="allowScriptAccess" value="sameDomain" />
				<!--<![endif]-->
					<a href="http://www.adobe.com/go/getflash">
						<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
					</a>
				<!--[if !IE]>-->
				</object>
				<!--<![endif]-->
			</object>
		</div>
		<div id="instrucciones">
			<p>Para sanar al paciente,
				lanza la medicina al interior de los vasos usando la resortera formada por las jeringas</p>
			<img src="img/salvaalpaciente.gif" width="450">
		</div>
	</body>
</html>
