﻿/*******************
written by Serasker in September 2012
http://activeden.net/user/serasker...
*/
package  {
	
	import flash.external.ExternalInterface;
	
	//standart flash library....
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	//tween classes...
	import com.greensock.*;
	import com.greensock.easing.*;
	
		
		
	//importing box2d classes...
	import Box2D.Dynamics.b2World;
	import Box2D.Common.Math.b2Vec2;
	import Box2D.Dynamics.b2Body;
	import Box2D.Dynamics.b2BodyDef;
	import Box2D.Collision.Shapes.b2PolygonShape;
	import Box2D.Dynamics.b2FixtureDef;
	import Box2D.Dynamics.b2DebugDraw;
	import Box2D.Dynamics.b2ContactListener;
	import flash.media.SoundChannel;
	import flash.xml.XMLNode;
	
	
	public class Main extends MovieClip {
		
		private var mainXML:XML;
		private var xmlLoader:URLLoader;
		
		
		private var entryPage:EntryPage;
		
		private var kitchen:Kitchen;
		private var sw:Number;
		private var sh:Number;
		
		//creating world object..
		private var world:b2World;
		private var gravity:b2Vec2;//gravity of the world..
		private var doSleep:Boolean;
		public const meterRatio:int=20;
		
		//our wall bodies...
		private var tableTopBody:b2Body;
		private var rightWallBody:b2Body;
		
		// knife objects...
		private var  knife1:Knife;
		private var  knife2:Knife;
		
		private var line1:Line1;
		private var line2:Line2;
		
		//catapult pocket...
		private var catapultPocket:CatapultPocket;
		
		//sugar data and body 
		private var sugar:Sugar;
		private var sugarBody:b2Body;
		
		// open waterGlass and waterGlassBody..
		private var openWaterGlass:OpenWaterGlass;
		private var openWaterGlassBody:b2Body;
		
		// closed waterGlass and waterGlassBody..
		private var closeWaterGlass:CloseWaterGlass;
		private var closeWaterGlassBody:b2Body;
		
		//shelf and shelf body ...
		private var shelf:Shelf;
		private var shelfBody:b2Body;
		
		
		
		//adding contact listener to world physic events...
		private var myContactListener:MyContactListener=new MyContactListener();
		
		//creating gameLevel...
		private var gameLevel:uint=1;
	
		//sugar in or out of glass..
		private var sugarInGlass:Boolean;
		
		//defining arrays 
		public var openWaterGlasses:Vector.<MovieClip>=new Vector.<MovieClip>;
		private var openWaterGlassBodies:Vector.<b2Body>=new Vector.<b2Body>;
		
		private var closeWaterGlasses:Vector.<MovieClip>=new Vector.<MovieClip>;
		private var closeWaterGlassBodies:Vector.<b2Body>=new Vector.<b2Body>;
		
		private var shelfs:Vector.<MovieClip>=new Vector.<MovieClip>;
		private var shelfBodies:Vector.<b2Body>=new Vector.<b2Body>;
		
		//sugarCount using in every level..
		private var sugarCount:uint;
		
		//sugarBoard using in game..
		private var boardSugar:Sugar;
		private var sugarBoard:MovieClip;
		private var boardSugars:Vector.<MovieClip>=new Vector.<MovieClip>;
		
		private var targetCount:uint;
		
		private var playAgain:PlayAgain;
		private var nextLevel:NextLevel;
		
		private var levelClip:LevelClip;
		
		//Variable para contar el puntaje del jugador
		private var score:int=0;
		
		private var scoreClip:ScoreClip;
		
		
		
		
		
		
		
		
		
		public function Main() {
			//init to put items to the stage ..
			init();
			
		}//constructor
		
		
		private function init():void
		{
			sw=stage.stageWidth;
			sh=stage.stageHeight;
			
			//read XML file....
			parseXML("XML/main.xml");
			
			 
			
		}//init
		
		private function parseXML(xmlPath:String):void
		{

			var XMLRequest:URLRequest = new URLRequest(xmlPath);

			xmlLoader = new URLLoader  ;

			xmlLoader.addEventListener(Event.COMPLETE,xmlLoaded);

			xmlLoader.load(XMLRequest);

		}//parseXML;
		
		private function xmlLoaded(e:Event):void
		{
			//xml isready to use ..
			mainXML=new XML(xmlLoader.data)
			
			//trace(node.child("openWaterGlass").children()[0].@posx);
			
			//adding entryPage
			entryPage=new EntryPage();
			addChild(entryPage);
			entryPage.x=sw/2;
			entryPage.y=sh/2;
			
					
			
		}//xmlloaded
		
		public  function playGame():void
		{
			ExternalInterface.call("actualizarJuego", 1);
			prepareKitchen();//kitchen drawing..
			prepareKitchenWorld();//preparing kitchen world for physic events of  box2d object 
			
			 prepareWall(tableTopBody,0,297,700,5);
			 prepareWall(rightWallBody,697.5,0,2.5,292);
			
			 addLevelClip();
			 addScoreClip();
			 addThrowingPlatform();
				
			//creating game objects for apropriate level...
			createLevel(gameLevel);
			
			 
			
			 
			// addWaterGlass();
			 
			//drawItems();//drawing items for simulating...
			
		}//playGame
		
		
		private function addLevelClip():void
		{
			
			levelClip=new LevelClip();
			addChild(levelClip);
			levelClip.x=75;
			levelClip.y=300;
			levelClip.level_txt.text=String(gameLevel);
			
		}
		
		private function addScoreClip():void {
			scoreClip=new ScoreClip();
			addChild(scoreClip);
			scoreClip.x=290;
			scoreClip.y=300;
			scoreClip.level_txt.text=String(score);
		}
		
		
		private function createLevel(level:uint):void
		{
			//dedecting openWaterglasses....
			var node:XML=mainXML.levels.children()[level-1];
			sugarCount=uint(node.@sugars);
			targetCount=uint(node.@targets);
			prepareSugarBoard(sugarCount);
			
				if(node.@useShelf=="true")
				{
					//getting openedglass counts for use in game 
					var _shelfCount:uint=uint(node.@shelfCount);
					var shelfNode:XMLList=node.child("shelf");
					for(var k:uint; k<_shelfCount; ++k)
					{
						addShelf(Number(shelfNode.children()[k].@posy),Number(shelfNode.children()[k].@width));
						
					}
					
				}//if
				
				
					
				
				if(node.@useOpenWaterGlass=="true")
				{
					//getting openedglass counts for use in game 
					var _openWaterGlassCount:uint=uint(node.@openWaterGlassCount);
					var openWaterGlassPositionNode:XMLList=node.child("openWaterGlass");
					for(var i:uint; i<_openWaterGlassCount; ++i)
					{
						addOpenWaterGlass(Number(openWaterGlassPositionNode.children()[i].@posx),Number(openWaterGlassPositionNode.children()[i].@posy));
						
					}
					
				}//if
				
				if(node.@useCloseWaterGlass=="true")
				{
					//getting openedglass counts for use in game 
					var _closeWaterGlassCount:uint=uint(node.@closeWaterGlassCount);
					var closeWaterGlassPositionNode:XMLList=node.child("closeWaterGlass");
					for(var j:uint; j<_closeWaterGlassCount; ++j)
					{
						addCloseWaterGlass(Number(closeWaterGlassPositionNode.children()[j].@posx),Number(closeWaterGlassPositionNode.children()[j].@posy));
						
					}
					
				}//if
				
				 addSugar();//adding sugar to the stage for throwing ....
				
						
			
		}//create level
		
		private function prepareSugarBoard(sugarCount:uint):void
		{
			
			//adding sugarBoard
			sugarBoard=new MovieClip();
			addChild(sugarBoard);
			sugarBoard.y=380;
		for(var i:uint=0; i<sugarCount-1; ++i)
		{
			boardSugar=new Sugar();
			sugarBoard.addChild(boardSugar);
			boardSugar.x=boardSugar.width+(i*20);
			boardSugars.push(boardSugar);
			
		}
		
			
			
			
		}//preparesugarcount

		
		
		private function prepareKitchen():void
		{
			
			kitchen=new Kitchen();
			addChild(kitchen);
			kitchen.x=sw/2;
			kitchen.y=sh/2;
			
			
			
		}//function 
		
		
		private function prepareKitchenWorld():void
		{
			gravity=new b2Vec2(0,9.8);
			world=new b2World(gravity,doSleep);
			world.SetContactListener(myContactListener);
			
			
		}//preparekitchenworld
		
		private function prepareWall(wallBody:b2Body,wallx:Number,wally:Number,wallWidth:Number,wallHeight:Number):void
		{
			
			
			//defining our body definitio..
			var wallBodyDef:b2BodyDef=new b2BodyDef();
			wallBodyDef.position.Set(wallx/meterRatio,wally/meterRatio);
			wallBodyDef.type=b2Body.b2_staticBody;
					
			//preparing  soul of our wallbody(shape and fixture definitions..)
			var wallShape:b2PolygonShape=new b2PolygonShape();
			wallShape.SetAsBox(wallWidth/meterRatio,wallHeight/meterRatio);
			
			
			var wallFixtureDef:b2FixtureDef=new b2FixtureDef();
			wallFixtureDef.density=1;
			wallFixtureDef.friction=0.5;
			wallFixtureDef.restitution=0.4;
			wallFixtureDef.shape=wallShape;
						
			//creating wall body and fixture...
			
			wallBody=world.CreateBody(wallBodyDef);
			wallBody.CreateFixture(wallFixtureDef);
			
			
		}//function
		
		
		
		private function addThrowingPlatform():void
		{
			
			knife1=new Knife();
			addChild(knife1);
			
			knife2=new Knife();
			addChild(knife2);
			
			knife2.x=80;
			knife1.x=95;
			
			knife2.y=242;
			knife1.y=225;
			
			
			
			line1=new Line1();
			addChild(line1);
			line1.x=93;
			line1.y=190;
					
			
			line2=new Line2();
			addChild(line2);
			line2.x=83;
			line2.y=207;
			
			line1.rotation=160;
			line2.scaleX=0;
			
			catapultPocket=new CatapultPocket();
			addChild(catapultPocket);
			catapultPocket.x=82;
			catapultPocket.y=198;
			
						
		}//function 
		
		private function resetThrowingItems():void
		{
			TweenLite.to(line2,.2,{scaleX:0});
			TweenLite.to(line1,.2,{scaleX:0});
			TweenLite.to(catapultPocket,.2,{x:82,y:198,rotation:0});
			//line2.scaleX=0;
			//line1.scaleX=1;
			//line1.rotation=160;
			
			
		}
		
		
		private function addSugar():void
		{
			
			sugar=new Sugar();
			addChild(sugar);
			sugar.x=86;
			sugar.y=198;
			sugar.buttonMode=true;
			
			
			
			
			this.swapChildren(sugar,catapultPocket);
			this.setChildIndex(line2,this.numChildren-1);
			this.setChildIndex(knife2,this.numChildren-1);
			sugarInGlass=false;
			
			
			//adding event to sugar for throwing..
			sugar.addEventListener(MouseEvent.MOUSE_DOWN,sugarClicked);
					
		}//addsugar
		
		
		
		private function sugarClicked(e:MouseEvent):void
		{
			
			addEventListener(MouseEvent.MOUSE_MOVE,dragSugar);
			addEventListener(MouseEvent.MOUSE_UP,throwSugar);
			sugar.removeEventListener(MouseEvent.MOUSE_DOWN,sugarClicked);
			
			
		}//sugarClicked
		
		
		
		private function dragSugar(e:MouseEvent):void
		{
			sugar.x=mouseX;
			sugar.y=mouseY;
			
			
			//calculating distance x and y positions...
			var disx:Number=sugar.x-86;
			var disy:Number=sugar.y-198;
				
			
			var ang:Number=(Math.atan2(disy,disx)*180/Math.PI)+180;
			sugar.rotation=Math.atan2(disy,disx)*180/Math.PI;
			
			
			catapultPocket.x=mouseX+(-6*Math.cos(ang*Math.PI/180));
			catapultPocket.y=mouseY+(-6*Math.sin(ang*Math.PI/180));
			catapultPocket.rotation=ang;
			
			var dlx:Number=catapultPocket.x-line1.x;
			var dly:Number=catapultPocket.y-line1.y;
			line1.scaleX=Math.sqrt((dlx * dlx ) +(dly * dly))/15;
			
			
			var dl2x:Number=catapultPocket.x-line2.x;
			var dl2y:Number=catapultPocket.y-line2.y;
			line2.scaleX=Math.sqrt((dl2x * dl2x ) +(dl2y * dl2y))/15;
			
			
			var line1angle:Number=(Math.atan2(dly,dlx)*180/Math.PI);
			line1.rotation=line1angle;
			
			var line2angle:Number=(Math.atan2(dl2y,dl2x)*180/Math.PI);
			line2.rotation=line2angle;
			
			
			
				
			
			
			
			if((disx * disx)+(disy * disy)>6400)
			{
				var sugarAngle:Number=Math.atan2(disy,disx);
				sugar.x=86+(80 * Math.cos(sugarAngle));
				sugar.y=198+(80 * Math.sin(sugarAngle));
				sugar.rotation=sugarAngle*180/Math.PI;
				var catAngle:Number=(sugarAngle*180/Math.PI)+180;
				catapultPocket.x=86+(-86*Math.cos(catAngle*Math.PI/180));
				catapultPocket.y=198+(-86*Math.sin(catAngle*Math.PI/180));
				catapultPocket.rotation=catAngle;
				
				var ln1x:Number=catapultPocket.x-line1.x;
				var ln1y:Number=catapultPocket.y-line1.y;
				
				line1.scaleX=Math.sqrt((ln1x * ln1x ) +(ln1y * ln1y))/15;
				
				var ln2x:Number=catapultPocket.x-line2.x;
				var ln2y:Number=catapultPocket.y-line2.y;
				
				line2.scaleX=Math.sqrt((ln2x * ln2x ) +(ln2y * ln2y))/15;
				
				
									
				
			}
			
			
		}//dragSugar
		
		
		private function throwSugar(e:MouseEvent):void
		{
			resetThrowingItems();
			 this.addEventListener("enterFrame",updateScreen);
			sugar.buttonMode=false;
			this.setChildIndex(sugar,this.numChildren-1);
			//removing listeners..
			removeEventListener(MouseEvent.MOUSE_MOVE,dragSugar);
			removeEventListener(MouseEvent.MOUSE_UP,throwSugar);
			
			var sugarBodyDef:b2BodyDef=new b2BodyDef();
			sugarBodyDef.position.Set(sugar.x/meterRatio,sugar.y/meterRatio);
			sugarBodyDef.type= b2Body.b2_dynamicBody;
			sugarBodyDef.userData=sugar;
			
			
			var sugarShape:b2PolygonShape=new b2PolygonShape();
			sugarShape.SetAsBox(7.5/meterRatio,7.5/meterRatio);
			
			var sugarFixtureDef:b2FixtureDef=new b2FixtureDef();
			sugarFixtureDef.density=1;
			sugarFixtureDef.friction=0.3;
			sugarFixtureDef.restitution=0.3;
			sugarFixtureDef.shape=sugarShape;
			
			sugarBody=world.CreateBody(sugarBodyDef);
			sugarBody.CreateFixture(sugarFixtureDef);
			
			
			var disx:Number=sugar.x-90;
			var disy:Number=sugar.y-217;
			var angle:Number=Math.atan2(disy,disx);
			var dist:Number=Math.sqrt((disx * disx)+(disy * disy));
			
			sugarBody.SetLinearVelocity(new b2Vec2(-dist*Math.cos(angle)/3,-dist*Math.sin(angle)/3));
			sugarBody.SetAngle(angle);
			
			
			
			
		}//throwSugar
		
		
		private function manageItems():void
		{
			if(sugarBody!=null && sugarBody.GetUserData()!=null)
			{
				sugarBody.GetUserData().x=sugarBody.GetPosition().x*meterRatio;
				sugarBody.GetUserData().y=sugarBody.GetPosition().y*meterRatio;
				sugarBody.GetUserData().rotation=sugarBody.GetAngle()*(180/Math.PI);
				
				if( (Math.abs(sugarBody.GetLinearVelocity().x)<0.0000000000001 &&  Math.abs(sugarBody.GetLinearVelocity().y)<0.0000000000001)
				   ||(sugarBody.GetPosition().x<0 || (sugarBody.GetPosition().x>700/meterRatio)))
			{
				
				this.removeEventListener("enterFrame",updateScreen);
				for(var i:uint=0; i<openWaterGlassBodies.length; ++i)
				{
					
					if(sugarBody.GetUserData().hitTestObject(openWaterGlassBodies[i].GetUserData().hitClip))
				   {
					  
					   sugarInGlass=true;
					   score += 5;
					   scoreClip.level_txt.text=String(score);
					   var winSound:WinSound=new WinSound();
					   var winChannel:SoundChannel=winSound.play();
					   removeChild(MovieClip(sugarBody.GetUserData()));
					   world.DestroyBody(sugarBody);
					   
					   openWaterGlassBodies[i].GetUserData().gotoAndPlay("remove")
					   world.DestroyBody(openWaterGlassBodies[i]);
					   openWaterGlassBodies.splice(i,1);
					   sugarCount--;
					   targetCount--;
					   break;
												
				   }
				}//for
				   
				   
				  if(sugarInGlass==false)
				   {
					  
						 var loseSound:LoseSound=new LoseSound();
						 var loseChannel:SoundChannel=loseSound.play();
					    removeChild(MovieClip(sugarBody.GetUserData()));
						world.DestroyBody(sugarBody);
						sugarCount--;
						controlGamePosition();
																	   
				   }
				   
				
						
				
				}//if
							
			}//if
						
		}//manageSugar
		
		
		public function controlGamePosition():void
		{
			
			if(sugarCount>0)
			{
				if(targetCount>0)
				{
					sugarBoard.removeChild(boardSugars[sugarCount-1]);
					addSugar();
				}
				else
				{
					playNextLevel();
				}
					
			}
			else
			{
				if(targetCount==0)
				{
					playNextLevel();
				}
				else
				{
					gameIsOver();
				}
				
			}
			
			
			
		}//controlgamepos
		
		
		private function gameIsOver():void
		{
			
			ExternalInterface.call("mandarDatos", score);
			playAgain=new PlayAgain();
			addChild(playAgain);
			playAgain.x=sw/2;
			playAgain.y=sh/2;
			playAgain.addEventListener("click",playAgainGame);
			
			
						
		}//gameisover...
		
		
		private function playNextLevel():void
		{
			nextLevel=new NextLevel();
			addChild(nextLevel);
			nextLevel.x=sw/2;
			nextLevel.y=sh/2;
			nextLevel.addEventListener("click",nextLevelGame);
			
		}//nextLevel
		
		private function nextLevelGame(e:MouseEvent):void
		{
			nextLevel.removeEventListener("click",nextLevelGame);
			removeChild(nextLevel);
			
			
			clearClips();
			clearBodies();
			gameLevel++;
			levelClip.level_txt.text=String(gameLevel);
			if(gameLevel<=10)
			{
				createLevel(gameLevel);
			}
			else
			{
				showDemoAnimation();
			}
			
			
						
			
		}//playagaingame
		
		private function showDemoAnimation():void
		{
			var demoAlert:DemoAlert=new DemoAlert();
			addChild(demoAlert);
			demoAlert.x=sw/2;
			demoAlert.y=sh/2;
			
		}//showdemoanimation..
		
		
		
		private function playAgainGame(e:MouseEvent):void
		{
			
			ExternalInterface.call("actualizarJuego", 1);
			score = 0;
			scoreClip.level_txt.text=String(score);
			playAgain.removeEventListener("click",playAgainGame);
			removeChild(playAgain);
			clearClips();
			clearBodies();
			createLevel(gameLevel);
			
						
			
		}//playagaingame
		
		private function clearClips():void
		{
			if(openWaterGlasses.length>0)
			{
				for(var i:uint=0; i<openWaterGlasses.length; ++i)
				{
					
					removeChild(openWaterGlasses[i]);
				}
				openWaterGlasses.length=0;
				
			}
			if(closeWaterGlasses.length>0)
			{
				for(var j:uint=0; j<closeWaterGlasses.length; ++j)
				{
					
					removeChild(closeWaterGlasses[j]);
				}
				closeWaterGlasses.length=0;
				
			}
			
			if(shelfs.length>0)
			{
				for(var k:uint=0; k<shelfs.length; ++k)
				{
					
					removeChild(shelfs[k]);
				}
				shelfs.length=0;
				
			}
			
				boardSugars.length=0;
				removeChild(sugarBoard);
			
			
		}//clearClips
		
		
		
		private function clearBodies():void
		{
			
			if(openWaterGlassBodies.length>0)
			{
				for(var i:uint=0; i<openWaterGlassBodies.length; ++i)
				{
					
					world.DestroyBody(openWaterGlassBodies[i]);
				}
				openWaterGlassBodies.length=0;
				
			}
			if(closeWaterGlassBodies.length>0)
			{
				for(var j:uint=0; j<closeWaterGlassBodies.length; ++j)
				{
					
					world.DestroyBody(closeWaterGlassBodies[j]);
				}
				closeWaterGlassBodies.length=0;
				
			}
			
			if(shelfBodies.length>0)
			{
				for(var k:uint=0; k<shelfBodies.length; ++k)
				{
					
					world.DestroyBody(shelfBodies[k]);
				}
				shelfBodies.length=0;
				
			}
			
		}//clearBodies..
		
		private function addOpenWaterGlass(posx:Number,posy:Number):void
		{
			
			//adding waterGlass clip to stage..
			openWaterGlass=new OpenWaterGlass();
			addChild(openWaterGlass);
			openWaterGlass.x=posx;
			openWaterGlass.y=posy;
			
			openWaterGlasses.push(openWaterGlass);
			
			//defining bodydef of bottle..
			var openWaterGlassBodyDef:b2BodyDef=new b2BodyDef();
			openWaterGlassBodyDef.position.Set(posx/meterRatio,posy/meterRatio);
			openWaterGlassBodyDef.type=b2Body.b2_staticBody
			openWaterGlassBodyDef.userData=openWaterGlass;
			
			openWaterGlassBody=world.CreateBody(openWaterGlassBodyDef);
			openWaterGlassBodies.push(openWaterGlassBody);
					
			//preparing  soul of our bottlebody(shape and fixture definitions..)
			
			//3 fixture will be in 1 body..
			var edgeFixtureDef:b2FixtureDef=new b2FixtureDef();
			edgeFixtureDef.density=1;
			edgeFixtureDef.friction=0.1;
			edgeFixtureDef.restitution=0.5;
			
			
			var edgeGlassShape:b2PolygonShape=new b2PolygonShape();
			edgeGlassShape.SetAsOrientedBox(1/meterRatio,27.5/meterRatio,new b2Vec2(-20/meterRatio,0),0);
			edgeFixtureDef.shape=edgeGlassShape;
			openWaterGlassBody.CreateFixture(edgeFixtureDef);
			
			edgeGlassShape.SetAsOrientedBox(20/meterRatio,3/meterRatio,new b2Vec2(0,26/meterRatio),0);
			edgeFixtureDef.shape=edgeGlassShape;
			openWaterGlassBody.CreateFixture(edgeFixtureDef);
			
			edgeGlassShape.SetAsOrientedBox(1/meterRatio,27.5/meterRatio,new b2Vec2(20/meterRatio,0),0);
			edgeFixtureDef.shape=edgeGlassShape;
			openWaterGlassBody.CreateFixture(edgeFixtureDef);
			
						
		}//addOpenWaterGlass
		
		
		private function addCloseWaterGlass(posx:Number,posy:Number):void
		{
			
			//adding waterGlass clip to stage..
			closeWaterGlass=new CloseWaterGlass();
			addChild(closeWaterGlass);
			closeWaterGlass.x=posx;
			closeWaterGlass.y=posy;
			
			closeWaterGlasses.push(closeWaterGlass);
			
			//defining bodydef of bottle..
			var closeWaterGlassBodyDef:b2BodyDef=new b2BodyDef();
			closeWaterGlassBodyDef.position.Set(posx/meterRatio,posy/meterRatio);
			closeWaterGlassBodyDef.type=b2Body.b2_staticBody
			closeWaterGlassBodyDef.userData=closeWaterGlass;
			
			closeWaterGlassBody=world.CreateBody(closeWaterGlassBodyDef);
			closeWaterGlassBodies.push(closeWaterGlassBody);
					
			//preparing  soul of our bottlebody(shape and fixture definitions..)
			
			//3 fixture will be in 1 body..
			var edgeFixtureDef:b2FixtureDef=new b2FixtureDef();
			edgeFixtureDef.density=1;
			edgeFixtureDef.friction=0.1;
			edgeFixtureDef.restitution=0.5;
			
			
			var edgeGlassShape:b2PolygonShape=new b2PolygonShape();
			edgeGlassShape.SetAsOrientedBox(1/meterRatio,27.5/meterRatio,new b2Vec2(-20/meterRatio,0),0);
			edgeFixtureDef.shape=edgeGlassShape;
			closeWaterGlassBody.CreateFixture(edgeFixtureDef);
			
			edgeGlassShape.SetAsOrientedBox(20/meterRatio,3/meterRatio,new b2Vec2(0,-26/meterRatio),0);
			edgeFixtureDef.shape=edgeGlassShape;
			closeWaterGlassBody.CreateFixture(edgeFixtureDef);
			
			edgeGlassShape.SetAsOrientedBox(1/meterRatio,27.5/meterRatio,new b2Vec2(20/meterRatio,0),0);
			edgeFixtureDef.shape=edgeGlassShape;
			closeWaterGlassBody.CreateFixture(edgeFixtureDef);
			
						
		}//addCloseWaterGlass
		
		private function addShelf(posy:Number,shelfWidth:Number):void
		{
			
			//adding shelf clip to stage..
			shelf=new Shelf();
			addChild(shelf);
			shelf.width=shelfWidth;
			shelf.x=sw-(shelf.width/2)-5;
			shelf.y=posy;
			
			
			shelfs.push(shelf);
			
			//defining bodydef of  a shelf..
			var shelfBodyDef:b2BodyDef=new b2BodyDef();
			 shelfBodyDef.position.Set(shelf.x/meterRatio,posy/meterRatio);
			 shelfBodyDef.type=b2Body.b2_staticBody
			 shelfBodyDef.userData= null;
			
			 shelfBody=world.CreateBody( shelfBodyDef);
			 shelfBodies.push( shelfBody);
					
					
			
			
			
			
			//preparing  soul of our  shelfbody(shape and fixture definitions..)
			
			//3 fixture will be in 1 body..
			var edgeFixtureDef:b2FixtureDef=new b2FixtureDef();
			edgeFixtureDef.density=1;
			edgeFixtureDef.friction=0.1;
			edgeFixtureDef.restitution=0.5;
			
			
			var edgeGlassShape:b2PolygonShape=new b2PolygonShape();
			edgeGlassShape.SetAsOrientedBox((shelfWidth/2)/meterRatio,1/meterRatio,new b2Vec2(0,3.5/meterRatio),0);
			edgeFixtureDef.shape=edgeGlassShape;
			 shelfBody.CreateFixture(edgeFixtureDef);
						 
			 edgeGlassShape.SetAsOrientedBox((shelfWidth/2)/meterRatio,1/meterRatio,new b2Vec2(0,13.5/meterRatio),0);
			edgeFixtureDef.shape=edgeGlassShape;
			 shelfBody.CreateFixture(edgeFixtureDef);
			
			
		}//addShelf
		
		
		
		
		
		
		private function drawItems():void
		{
			var sp:Sprite = new Sprite()  ;
			addChild(sp);

			var dbDraw:b2DebugDraw = new b2DebugDraw  ;
			dbDraw.SetSprite(sp);
			dbDraw.SetDrawScale(meterRatio);
			dbDraw.SetFlags(b2DebugDraw.e_shapeBit | b2DebugDraw.e_jointBit );
			dbDraw.SetLineThickness(0);
			dbDraw.SetAlpha(0);
			dbDraw.SetFillAlpha(1);
			world.SetDebugDraw(dbDraw);
			world.DrawDebugData();
			
		}//drawItems
		
		private function updateScreen(e:Event):void
		{
			
			manageItems();
					
			
			world.Step(1/30,10,10);
			world.ClearForces();
			world.DrawDebugData();
		}//updateScreen
		
		
		
		
	}//class
	
}//package
