﻿package 
{

	import Box2D.Dynamics.b2ContactListener;
	import Box2D.Dynamics.Contacts.b2Contact;
	import Box2D.Dynamics.b2Body;

	import flash.media.Sound;
	import flash.media.SoundChannel;
	import Box2D.Dynamics.b2ContactImpulse;


	public class MyContactListener extends b2ContactListener
	{
		private var openWaterGlass:OpenWaterGlass;
		private var contactSound:ContactSound;
		private var channel:SoundChannel;
		private var glassSound:GlassSound;
		private var glassChannel:SoundChannel;
		public function MyContactListener()
		{
			// constructor code
		}

		override public function BeginContact(bc:b2Contact):void
		{



			if (bc.GetFixtureB().GetBody().GetUserData() != null)
			{
				glassSound=new GlassSound();
				glassChannel = glassSound.play();

			}
			else
			{
				contactSound=new ContactSound();
				channel = contactSound.play();

			}



		}

		override public function PostSolve(bc:b2Contact,bi:b2ContactImpulse):void
		{
			
				if (bc.GetFixtureB().GetBody().GetUserData() == openWaterGlass && bi.normalImpulses[0] < 1 )
				{
					if (glassChannel != null)
					{
						glassChannel.stop();
					}
				}
				
				if (bc.GetFixtureB().GetBody().GetUserData() == null && bi.normalImpulses[0] < 2 )
				{
					if (channel != null)
					{
						channel.stop();
					}
				}
					

				


		}

	}//class

}//package