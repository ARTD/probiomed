<!DOCTYPE html>
<html lang="es-mx">
<head>
  <meta charset="utf-8" />
	<title>Sopa de Letras</title>
  <link type="image/x-icon" href="img/favicon.ico" rel="icon"/>
  <link rel="stylesheet" type="text/css" href="css/estilos.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
  <!--[if lt IE 9]>
      <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <! [endif] -->
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
  <script>
    window.jQuery || document.write("<script src='js/jquery.min.js'><\/script>");
  </script>
  <script src="js/wordfind.js"></script>
  <script src="js/wordfindgame.js"></script>
</head>
<body>
  <?php
    require ("../../funciones.php");
    include('../../conexion.php');

    // Esta variable sirve para generar un random entre las tres primeras especialidades
    $especial = rand(1,3);
    // Esta variable sirve para definir la marca a escoger
    $marca = null;
    // Esta variable sirve para que una marca siempre esté presente
    $palabra = null;
    // Esta variable sirve para buscar la relación en la base de datos
    $ind = 0;

    // Se hace una comprobación de que si la especialidad es 1, se busca el primer valor en la base de datos
    if ($especial == 1) {
      // Se genera un random entre las marcas que existen de esa especialidad
      $marca = rand(1,4);
      // Se decide por la palabra que se va a utilizar conforme al random, y se toma el indicador para poder encontrar las palabras en la base de datos
      switch ($marca) {
        case 1:
          $palabra = "URILAST";
          $ind = 1;
          break;
        case 2:
          $palabra = "TACEX";
          $ind = 2;
          break;
        case 3:
          $palabra = "OVELQUIN";
          $ind = 3;
          break;
        case 4:
          $palabra = "GALEDOL";
          $ind = 4;
          break;
      }
    }
    // Se hace una comprobación de que si la especialidad es 2, se busca el segundo valor en la base de datos
    elseif ($especial == 2) {
      // Se genera un random entre las marcas que existen de esa especialidad
      $marca = rand(1,6);
      // Se decide por la palabra que se va a utilizar conforme al random, y se toma el indicador para poder encontrar las palabras en la base de datos
      switch ($marca) {
        case 1:
          $palabra = "TIRAPROB";
          $ind = 5;
          break;
        case 2:
          $palabra = "BAPEX";
          $ind = 6;
          break;
        case 3:
          $palabra = "PROTALGINE";
          $ind = 7;
          break;
        case 4:
          $palabra = "PROSERTIN";
          $ind = 8;
          break;
        case 5:
          $palabra = "ERGOCAF";
          $ind = 9;
          break;
        case 6:
          $palabra = "SILDEREC";
          $ind = 10;
          break;
      }
    }
    // En caso de que el valor sea 3, se busca el tercer valor en la base de datos
    else{
      // Se genera un random entre las marcas que existen de esa especialidad
      $marca = rand(1,7);
      // Se decide por la palabra que se va a utilizar conforme al random, y se toma el indicador para poder encontrar las palabras en la base de datos
      switch ($marca) {
        case 1:
          $palabra = "VASCOL";
          $ind = 11;
          break;
        case 2:
          $palabra = "SAPRAME";
          $ind = 12;
          break;
        case 3:
          $palabra = "ARA2";
          $ind = 13;
          break;
        case 4:
          $palabra = "ROFUCAL";
          $ind = 14;
          break;
        case 5:
          $palabra = "AGLUMET";
          $ind = 15;
          break;
        case 6:
          $palabra = "GLINUX";
          $ind = 16;
          break;
        case 7:
          $palabra = "PROTOPHIN";
          $ind = 17;
          break;
      }
    }   

    // Se hace una insercion a la tabla de juegos para poder hacer un reporte sobre la frecuencia de los juegos
    mysqli_query($con,"UPDATE juegos SET veces_jugadas = veces_jugadas + 1 WHERE id_juegos = 1");

    $result = mysqli_query($con,"SELECT palabra FROM palabras WHERE id_marcas = $ind ORDER BY RAND()");
    $final = array();

    // Se navega en todo el array
    while ($rows = mysqli_fetch_array($result)) {
      $final[] = $rows['palabra'];
    }
      
    $user=$_SESSION['usuarios'];

    $sql = "SELECT usuario,puntos FROM usuarios WHERE id_usuarios = '$user'";
    $result = mysqli_query($con,$sql);
    
    $rows = mysqli_fetch_array($result);
    
  ?>

  <section id="contenedor">
      <article id="contenedorpuntos">
        <p>Usuario: <?php echo $rows['usuario'];?></p>
        <p>Puntos: <?php echo utf8_encode($rows['puntos']);?></p>
      </article>
      <article style="display: inline-block; width:11%;">
          <a id="linkregresar" href="../../contenido.php"><img id="regresar" src="img/PASTILLA.png"></a>
      </article>
  </section>

  <section id="cont_principal">
      <div id='puzzle'></div>
      <div id="contenedorpalabras">
        <div id="words"></div>
      </div>  
      <!-- <img id="extras" src="img/Extras.png"> -->
      <div><a href="index.php" target="_self"><button class="v_jugar">Volver a Jugar</button></a></div>
      <!-- <article id="contenedorimagen">
       -->  <img id="prueba" src="img/prueba.gif">
       <!-- </article> -->
  </section>
  <script>

    // Este pedazo de codigo es para cuando ya se haga la conexion a la base de datos
    var words = ["<?php echo $palabra; ?>","<?php echo $final[0]; ?>","<?php echo $final[1]; ?>","<?php echo $final[2]; ?>","<?php echo $final[3]; ?>","<?php echo $final[4]; ?>", "<?php echo $final[5]; ?>","<?php echo $final[6]; ?>","<?php echo $final[7]; ?>","<?php echo $final[8]; ?>","<?php echo $final[9]; ?>"];

    // start a word find game
    var gamePuzzle = wordfindgame.create(words, '#puzzle', '#words');

    $('#solve').click( function() {
      wordfindgame.solve(gamePuzzle, words);
    });

    // create just a puzzle, without filling in the blanks and print to console
    var puzzle = wordfind.newPuzzle(
      words, 
      {height: 33, width:33, fillBlanks: false}
    );
    wordfind.print(puzzle);
  </script>
</body>
</html>