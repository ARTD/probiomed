// Esta función sirve para determinar que tipo de httprequest necesita utilizar AJAX
function getRequest() {
    var req = false;
    try{
        // most browsers
        req = new XMLHttpRequest();
    } catch (e){
        // IE
        try{
            req = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            // try an older version
            try{
                req = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e){
                return false;
            }
        }
    }
    return req;
}

/*Esta funcion es llamada por el flash cuando se le indica que no se quiere volver a jugar
Su trabajo es recibir la puntuación del jugador y enviarla al php 'update.php',
el cual se encarga de tomar el usuario y aumentar sus puntos*/
function mandarDatos(texto) {
  var prueba = texto;
  var ajax = getRequest();
  ajax.onreadystatechange = function(){
      if(ajax.readyState == 4){
          actualizarPuntos();
      }
  }
  ajax.open("GET", "update.php?q="+prueba, true);
  ajax.send();
}

function actualizarJuego(texto) {
  var prueba = texto;
  var ajax = getRequest();
  ajax.onreadystatechange = function(){
      if(ajax.readyState == 4){
          actualizarPuntos();
      }
  }
  ajax.open("GET", "juego.php?q="+prueba, true);
  ajax.send();
}

function actualizarPuntos() {
  var ajax = getRequest();
  ajax.onreadystatechange = function(){
      if(ajax.readyState == 4){
          document.getElementById("puntos").innerHTML="Puntos: "+ajax.responseText;
      }
  }
  ajax.open("GET", "puntos.php", true);
  ajax.send();
}