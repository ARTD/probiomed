<?php
    include('../../conexion.php');

    // Se hace una insercion a la tabla de juegos para poder hacer un reporte sobre la frecuencia de los juegos
    mysqli_query($con,"UPDATE juegos SET veces_jugadas = veces_jugadas + 1 WHERE id_juegos = 2");

    // Se cierra la conexion para no volcar la memoria
    mysqli_close($con);
?>