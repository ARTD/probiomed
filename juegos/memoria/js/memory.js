(function($){
	// Variables set from the XML
	var front;						// the front graphic
	var bg;							// the background image
	var cardW;						// the width of one card
	var cardH;						// the height of one card
	var col;						// how many columns
	
	var revealspeed;				// the speed of the reveal flip
	var hidespeed;					// the speed of the flip back
	var fadespeed;					// the speed a match fades away
	var introspeed;					// the speed of the game fading in (doesn't work on IE)
	var endscreendelay;				// the delay after the last match before revealing your score
	var endscreenfade;				// the speed at which the score fades in
	
	// Variables used by the application == DON"T TOUCH
	var $xml;
	var totalCards;
	var cardsets;
	var currClickID;
	var cardOne;
	var cardTwo;
	var num_correct = 0;
	var attempts = 15;
	var clickready = true;
	var dataArray = [];
	var randomizer = [];
	var allCards = [];
	var boardHeight;
	var boardWidth;

	//Variable añadida por el programador
	var tiempoinicio = new Date();//Se crea una variable de tipo date, para poder trabajar con el tiempo
	tiempoinicio = Number(tiempoinicio.getTime());//Esta variable comienza a contar el tiempo.
	var now;
	var nt;
	var segundos;//Variable que guarda los segundos que tardo el jugador en terminar el juego
	var puntos;//Esta variable guarda los puntos que generó el jugador, al terminar el juego.
	var puntostotal;//Variable que guarda todo el puntaje total, incluyendo ls puntos adicionales que se dan al terminar el juego en cierto tiempo.
	
	// End Screen Variables
	var skillArray = [];
	var skillInc;
	var linetwo;
	var linetwopre;
	var linetwopost;
	var linetwosolo;
	var linethree;
	var linethreereplay;
	var linethreelink;

	var anchura = $(window).width();
	var altura = $(window).height();
	var anchuraPantalla;


	if (anchura <= 768) {
		anchura = 80;
		altura = 80;
		columns = 8;
		anchuraPantalla = 1000;
	};

	if (anchura > 768 && anchura <= 1024) {
		anchura = 100;
		altura = 100;
		columns = 8;
	};

	if (anchura > 1024) {
		anchura = 150;
		altura = 150;
		columns = 7;
		anchuraPantalla = 1100;
	};
	
	getXML();
	
	function getXML(){
		$.ajax({
			type: "GET",
			url: "memory.xml",
			dataType: "xml",
			success: xmlParser
			
		});
		}
	
	function xmlParser(xml) {
		
		$xml = $(xml);
	
		front = $xml.find("front").text();
		bg = $xml.find("bg").text();
		cardW = anchura;
		cardH = altura;
		
		revealspeed = Number($xml.find("revealspeed").text());
		hidespeed = Number($xml.find("hidespeed").text());
		fadespeed = Number($xml.find("fadespeed").text());
		introspeed = Number($xml.find("introspeed").text());
		endscreendelay = Number($xml.find("endscreendelay").text());
		endscreenfade = Number($xml.find("endscreenfade").text());
		
		skillArray = $xml.find("skills").text().split(',');
		skillInc = Number($xml.find("skillInc").text());
		
		linetwo = $xml.find("lineTwo").text();
		linetwopre = $xml.find("lineTwoPre").text();
		linetwopost = $xml.find("lineTwoPost").text();
		linetwosolo = $xml.find("lineTwoSolo").text();
		
		linethree = $xml.find("lineThree").text();
		linethreereplay = $xml.find("lineThreeReplay").text();
		linethreelink = $xml.find("lineThreeLink").text();
		
		
		var n = 0;
		$xml.find("card").each(function () {
			dataArray.push({'cardA':$(this).attr("cardA"),'cardB':$(this).attr("cardB"),'id':n});
			n++;
			
		});
		
		cardsets = dataArray.length;
		totalCards = cardsets * 2;
	
		for (var i=0;i<cardsets;i++){
			randomizer.push(i);
			randomizer.push(i+cardsets);
			allCards[i]=dataArray[i];
			allCards[i+cardsets]=(dataArray[i]);
			}
			
		shuffleData(randomizer);
		
		//create the remaining divs
		$('.memory').append('<div class="bg"></div>');
		$('.memory').append('<div class="cards"></div>');
		$('.memory').append('<div class="gameover"></div>');
		$('.gameover').append('<div class="gameover_bg"></div>');
		
		$(".memory").css({'width' : cardW * columns,'overflow':'auto'});
		var rows = Math.ceil(totalCards/columns);
		//5 is a quick layout fix
		$(".cards").css({'width' : anchuraPantalla,'overflow':'auto','height':(cardH * rows + 5) +'px'});
		$(".gameover").css({'width' : cardW * columns});
		
		buildgame();
	}

	function getRequest() {
    var req = false;
    try{
        // most browsers
        req = new XMLHttpRequest();
    } catch (e){
        // IE
        try{
            req = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            // try an older version
            try{
                req = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e){
                return false;
            }
        }
    }
    return req;
}

function mandarDatos() {
  var prueba = 1;
  var ajax = getRequest();
  ajax.onreadystatechange = function(){
      if(ajax.readyState == 4){
          //alert("Datos enviados satisfactoriamente");
      }
  }
  ajax.open("GET", "actualizaJuego.php?q="+ prueba, true);
  ajax.send();
}
	
function mandarPuntaje(puntos)
{
	var prueba = puntos;
	var ajax = getRequest();

	ajax.onreadystatechange = function(){
      if(ajax.readyState == 4){
          //alert("puntaje:" + prueba);
      }
  }
  ajax.open("GET", "mandarPuntaje.php?q="+ prueba, true);
  ajax.send();
}
	function shuffleData(arr){
		for(var j, x, i = arr.length; i; j = parseInt(Math.random() * i), x = arr[--i], arr[i] = arr[j], 	arr[j] = x);
		return arr;
	};
	
	function setClick(id_set,id,set){
		var _id = '.'+id_set;
		 $(_id).click(function(){
		  cardClick(id,set)
		 })
	}
	
	function buildgame(){
		var c_id;
		var set;
		
		for (i=0;i<totalCards;i++){
			if (randomizer[i]<cardsets){
				c_id = allCards[randomizer[i]].id;
				set = 'a';
				id_set = c_id+set;
				$(".cards").append('<div class="card" id="a'+c_id+'"><span class="'+id_set+'"><img class="cardFront" src="' + front + '" width="'+ cardW +'" height="'+ cardH +'"/></span><img class="cardBack" src="' + allCards[randomizer[i]].cardA + '" width="'+ cardW +'" height="'+ cardH +'"/></div>');
				setClick(id_set,c_id,set);
			} else{
				c_id = allCards[randomizer[i]].id;
				set = 'b';
				id_set = c_id+set;
				$(".cards").append('<div class="card" id="b'+c_id+'"><span class="'+id_set+'"><img class="cardFront" src="' + front + '" width="'+ cardW +'" height="'+ cardH +'"/></span><img class="cardBack" src="' + allCards[randomizer[i]].cardB + '" width="'+ cardW +'" height="'+ cardH +'"/></div>');
				setClick(id_set,c_id,set);
			}
			
		}
		
		if ($(".bg").children().length > 0) $(".bg_img").children().remove();
		$(".bg").append('<div class="bg_img"><img id="bg" src="' + bg + '"/></div>');

		$(".card").css({'float':'left','width' : cardW +'px', 'height':cardH+'px','position':'relative'});
		$(".cardFront").css({'position':'absolute'});
		$(".cardBack").css({'position':'abolute'});
		$(".bg_img").css({'position':'abolute'});

		
		$('.bg_img img').load(function(){
			$('.memory').fadeIn(introspeed); // Doesn't work in IE!! It simply appears
			boardHeight = $(".cards").height();
			boardWidth = $(".cards").width();
			//alert(boardWidth)
			$(".gameover").css({'height':$(".cards").height()});
		});
	}
	
	function cardClick(id,set){
		if (clickready){
			var card = 'div#'+set+id+'.card';
			spinCard(card);
			if (currClickID == null){
				currClickID = id;
				cardOne = card;
				document.f.intento.value = attempts--;
				

    			if(attempts == 0)
      			 {
       			  $("#perdiste").fadeIn(1100);

       			  $("#perdiste").click(function(){
       			  	$("#perdiste").fadeOut(1100);
       			  	  playAgainIntentos();
       			  });
       			 }

			}else {
				cardTwo = card;
				clickready = false;
				if (currClickID == id){
					//it is a match
					num_correct++

					//Este código se añadio para contar el puntaje y las coincidencias
					document.f.contador.value = "0";
					document.f.contador.value = num_correct;

					document.f.contador_puntos.value = "0"
					puntos = document.f.contador_puntos.value = num_correct * 1;
					//

					window.setTimeout(function() {
						fadeCards(cardOne);
						fadeCards(cardTwo);
						cardOne = cardTwo = undefined;
						currClickID = null;
						clickready = true;
					},1000);
					// you got em all
					if (num_correct == cardsets){
						window.setTimeout(function() {
							youwin();
							num_correct = 0;
							attempts = 15;

							//Código añadido por el programador (este código sirve para contar el tiempo en el que se resuelve el juego y añadir puntos al jugador).
							document.f.crono.value = "";
							if(num_correct = 0);
							{	
								now = new Date();
								nt = (now.getTime());
								segundos = Math.floor(.5+(nt - tiempoinicio)/1000);
								document.f.crono.value = segundos;
								//Esta condición revisa el tiempo en el que se tarda en terminar el juego y determina cuantos puntos extras se añaden al puntaje final.
								if (segundos <= 60)
								{
									puntostotal = puntos + 5;
									document.f.contador_puntos.value = puntostotal;
								}

								else if (segundos <= 90)
								{
									puntostotal = puntos + 3;
									document.f.contador_puntos.value = puntostotal;	
								}

								else if (segundos <= 120)

								{
									puntostotal = puntos;
									document.f.contador_puntos.value = puntostotal;
								}

								else
								{
									document.f.contador_puntos.value = puntostotal;
								}
							}
						},endscreendelay);
					}
				}else{
					//it is NOT a match
					window.setTimeout(function() {
						spinCardBack(cardOne);
						spinCardBack(cardTwo);
						cardOne = cardTwo = undefined;
						currClickID = null;
						clickready = true;	
					},1000);
				}
			}
		}
	}
	function youwin(){
		if ($(".gameover_bg").children().length > 0) $(".gameover_bg").children().remove();
		$(".gameover_bg").append('<h1>'+accessSkill()+'</h1>');
		if (linetwo == "attempts"){
			$(".gameover_bg").append('<p>'+linetwopre + linetwopost+'</p>');
		}else{
			$(".gameover_bg").append('<p>'+ linetwosolo+'</p>');
			}
			
		if (linethree == "replay"){
			$(".gameover_bg").append('<p id="onemoretime"><a>'+linethreereplay+'</a></p>');
			onemoretime();
		}else{
			$(".gameover_bg").append('<p id="onemoretime">'+linethreelink+'</p>');
			}
		
		$('.gameover').fadeIn(endscreenfade);
		var middle = .5 - ( ($(".gameover_bg").height() / boardHeight ) / 2);
		
		$(".gameover_bg").css({'top':middle*100+"%"})
		}
		
	function onemoretime(){
		 $('#onemoretime a').click(function(){
		  playAgain();
		 })
	}
		
	function accessSkill(){
		for (var i=0;i<skillArray.length;i++){
			if (attempts <= cardsets + (i*skillInc)) {return skillArray[i];}
		}
		return skillArray[skillArray.length-1];
		}

	function playAgain(){
		$(".cards").children().remove();
		toggleEnd('none');
		shuffleData(randomizer);
		mandarPuntaje(puntostotal);
		//Este codigo se añadió para limpiar las variables
		document.f.contador.value = "0";
		document.f.contador_puntos.value = "0";
		document.f.crono.value = "0";
		document.f.intento.value = "15";
		num_correct = 0;
		attempts = 15;
		tiempoinicio = new Date();
		tiempoinicio = Number(tiempoinicio.getTime());
		now = new Date();
		nt = 0;
		puntos = 0;
		mandarDatos();
		buildgame();
	}

	function playAgainIntentos()
	{
		$(".cards").children().remove();
		toggleEnd('none');
		shuffleData(randomizer);
		//Este codigo se añadió para limpiar las variables
		document.f.contador.value = "0";
		document.f.contador_puntos.value = "0";
		document.f.crono.value = "0";
		document.f.intento.value = "15";
		num_correct = 0;
		attempts = 15;
		tiempoinicio = new Date();
		tiempoinicio = Number(tiempoinicio.getTime());
		now = new Date();
		nt = 0;
		puntos = 0;
		buildgame();
	}

	function toggleEnd(dir){
		$(".gameover").css({'display':dir});
		}
	function spinCard(id){
		
		var margin = $(id + ' .cardFront').width()/2;
			$(id + ' .cardBack').stop().css({width:'0px',height:''+cardH+'px',marginLeft:''+margin+'px',opacity:'0.5'});
			$(id + ' .cardFront').stop().animate({width:'0px',height:''+cardH+'px',marginLeft:''+margin+'px',opacity:'0.5'},{duration:revealspeed});
			window.setTimeout(function() {
			$(id + ' .cardBack').stop().animate({width:''+cardW+'px',height:''+cardH+'px',marginLeft:'0px',opacity:'1'},{duration:revealspeed});
			},revealspeed);
		}
		
	function spinCardBack(id){
		
		var margin = $(id + ' .cardBack').width()/2;
			
			$(id + ' .cardBack').stop().animate({width:'0px',height:''+cardH+'px',marginLeft:''+margin+'px',opacity:'0.5'},{duration:hidespeed});
			window.setTimeout(function() {
			$(id + ' .cardFront').stop().animate({width:''+cardW+'px',height:''+cardH+'px',marginLeft:'0px',opacity:'1'},{duration:hidespeed});
			},hidespeed);
			
		}
		
	function fadeCards(id){
			$(id + ' .cardBack').stop().animate({opacity:'0'},{duration:fadespeed});
			window.setTimeout(function() {
			$(id + ' .cardFront').stop().animate({opacity:'0'},{duration:fadespeed});
			},fadespeed);
		}

})(jQuery);