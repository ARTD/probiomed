<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Memoria</title>
<link type="image/x-icon" href="images/favicon.ico" rel="icon"/>
<link rel="stylesheet" type="text/css" href="memory.css" />
<!-- <link rel="stylesheet" type="text/css" href="sexyalertbox.css"media="all" /><!-- Estilo del Sexy Alert --> 

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<! [endif] -->
	<script>
		window.jQuery || document.write("<script src='js/jquery-1.9.1.min.js'><\/script>");
	</script>

<script type="text/javascript" src="js/memory.js"></script>
<script type="text/javascript" src="js/efectos.js"></script>

</head>

<body>
<?php
		include ("../../conexion.php");

		//Se hace la inserción a la tabla juegos
		mysqli_query($con, "UPDATE juegos SET veces_jugadas = veces_jugadas + 1 WHERE id_juegos = 3");

		//Se cierra la conexión para evitar volcado de memoria
		mysqli_close($con);
 ?>

 	<?php
		require ("../../funciones.php");
		include ("../../conexion.php");

		$user=$_SESSION['usuarios'];

    	$sql = "SELECT usuario,puntos FROM usuarios WHERE id_usuarios = '$user'";
    	$result = mysqli_query($con,$sql);
		// Se navega en todo el array
		while ($rows = mysqli_fetch_array($result))
		{?>
 			<!-- Aqui se le indica a la computadora que tiene que imprimir los resultados del "id" en el campo "value" 
 			Y los resultados del campo "especialidad" serán puestos como el nombre seleccionable-->
 			<p class="usuario">Usuario: <?php echo $rows['usuario'];?></p><p class="puntos">Puntos: <?php echo utf8_encode($rows['puntos']);?></p>
 			<a id="regresar" href="../../contenido.php"><img id="pastilla" src="images/PASTILLA regresar.png"></a>
 		<?php
		mysqli_close($con);
		}
?>
	
<img id="perdiste" src="images/Lo sentimos.png"/>
	
<form id="detalles" name="f">
	<article id="cerebro">
		<img src="images/cerebro.png"/>
	</article>

	<article id="coincidencias">
		<input id="coincide" type="text" name="contador" value="0" size="2"/>
	</article>

	<article id="puntos">
		<input id="puntaje" type="text" name="contador_puntos" value="0" size="2"/>
	</article>

	<article id="tiempo">
		<input id="tiempo_juego" type="text" name="crono" value="0" size="2"/>
	</article>

	<article id="intentos">
		<input id="intentos_juego" type="text" name="intento" value ="15" size="2"/>
	</article>
</form>

<div class="memory"></div>
<div class="clear"></div>

</body>
</html>
