<!DOCTYPE hmtl>
<?php
require ("funciones.php");
include "conexion.php";

seguridadIndex();

$error = 0;
$registrar=0;
$perdido=0;

if(isset($_POST['registrar']))
{    
    $registrar = 1;
    $error = registrarUsuario(limpiar($_POST['nombre']), $_POST['apellidop'], $_POST['apellidom'], $_POST['email'], $_POST['especialidad'], $_POST['tipo'], $_POST['representante'], $_POST['user'], $_POST['pass']);   
}
else if(isset($_POST['login']))
{
    $recordarme=0;
    if(isset($_POST['recordarme']))$recordarme=1;
    $error = login(limpiar($_POST['user']), $_POST['pass'],$recordarme);
    if($error>0)
    {
        header("Location: contenido.php");
        exit();
    }  
}

if(isset($_POST['perdido']))
{    
    $registrar = 1;
    $error = passolvido(limpiar($_POST['email']));   

}


?>

<html lang="es-mx">
<head>
	<meta charset= "utf-8" />
	<title>Probiomed</title>
	<meta name="description" content="Sitio exclusivo para Probiomed" />
	<link type="image/x-icon" href="img/favicon.ico" rel="icon"/>
	<link rel="stylesheet" href="css/estilos.css" />
	<link rel="sitemap" type="application/xml" title="Sitemap" href="sitemap.xml" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
	<script>
		window.jQuery || document.write("<script src='js/jquery.min.js'><\/script>");
	</script>
	<script type="text/javascript" src="js/efectos.js"></script>
</head>
<body>
    <header id="cabecera">
		<img src="img/logo_pr.png" />
	</header>
	<a href="#" class="vent_emergente" id="login"></a>
	<section class="popup3">
		<form name="login" method="post" action="">
				<h2>Login</h2>
					<article>
						<label>Usuario:</label>
						<input type="text" id="user" name="user" placeholder="Escribe tu usuario" required pattern="[A-Za-zÑñÁáÉéÍíÓóÚúÜü\s]+" <?php if($registrar && $error>0) echo 'value="'.limpiar($_POST['user']).'"'; ?> />
					</article>
					<article>
						<label>Contraseña:</label>
						<input type="password" id="pass" name="pass" placeholder="Escribe tu contraseña" required  />
					</article>
					<article>
						<label for="recordarme">Recordarme: </label>
						<input type="checkbox" id="recordarme" name="recordarme" />
					</article>		
					<aside class="boton">	
						<input class="btn" type="submit" name="login" id="login" value="Aceptar" />
					</aside>
					<aside class="enlace">
						<a href="#join"> Registrate</a>
						<a href="#password"> ¿Has olvidado tu contraseña?</a>
					</aside>
					<a class="close" href="#close"></a>
					
					 <?php
           					 switch ($error) {
               					  case -1://login-error
               					     echo '<aside id="errores" class="error1"><p>Usuario o clave incorrecta.</p></aside>';
               					     break;
               					 case -2://registro-error
               					     echo '<aside id="errores" class="error2"><p>Error al registrarse. Usuario ya existente.</p></aside>';
               					     break;
               					 case -3://registro-advetencia
               					     echo '<aside id="errores" class="error3"><p>El usuario y la contraseña deben tener como mínimo 4 carácteres.</p></aside>';
               					     break;//correcto
               					 default:
               					     if($registrar) echo '<aside id="errores" class="error4"><p>Se ha registrado correctamente.</p></aside>';
               					     break; 
          						  }
						?>
		</form>
	 </section>
	
	<a href="#" class="vent_emergente" id="password"></a>
	<section class="popup4">
		<form name="login" method="post" action="">
				<h2>Recuperar Contraseña</h2>
					<article>
						<label>Email:</label>
						<input type="text" id="email" name="email" placeholder="Escribe tu correo electronico" required/>
					</article>
					<aside class="enlace2">
						<input class="btn" type="submit" name="perdido" id="perdido" value="Enviar" />
						<a href="#login">Login</a>
					</aside>
					<a class="close" href="#close"></a>
		</form>
	 </section>

	<a href="#" class="vent_emergente" id="join"></a>
	<section class="popup">
		<form name="login" method="post" action="">
		<h2>Registro</h2>
		<p>Cada uno de los campos son obligatorios.</p>
		<article>
			<label for="nombre">Nombre:<sup>(*)</sup></label>
			<input type="text" id="nombre" name="nombre" placeholder="Escribe tu nombre" autofocus="autofocus" required pattern="[A-Za-zÑñÁáÉéÍíÓóÚúÜü\s]+"/>
			</article>
		</article>

		<article>
			<label for="apellidop">Apellido Paterno:<sup>(*)</sup></label>
			<input type="text" id="apellidop" name="apellidop" placeholder="Escribe tu Apellido Paterno"  required pattern="[A-Za-zÑñÁáÉéÍíÓóÚúÜü\s]+"/>
		</article>

		<article>
			<label for="apellidom">Apellido Materno:<sup>(*)</sup></label>
			<input type="text" id="apellidom" name="apellidom" placeholder="Escribe tu Apellido Materno"  required pattern="[A-Za-zÑñÁáÉéÍíÓóÚúÜü\s]+"/>
		</article>

		<article>
			<label for="email">Email:<sup>(*)</sup></label>
			<input type="email" id="email" name="email" placeholder="Escribe tu correo electronico" required/>
		</article>

		<article>
			<label for="especialidad">Especialidad:<sup>(*)</sup></label>
			<select id="especialidad" name="especialidad" required>
			<?php 
					$sql = "select * from especialidad;";
        			$result = mysql_query($sql,$conexion);
        			while($row = mysql_fetch_row($result))
   						{
   							echo "<option value='".$row[0]."'>".$row[1]."";
   						}
        			?>
			</select>
		</article>

		<article>
			<label for="tipo" class="label_radio">Tipo:<sup>(*)</sup></label>
			<!--<input  type="radio" id="tipo" class="radio" name="tipo" value="1" required>Doctor
			<input  type="radio" id="tipo" class="radio" name="tipo" value="2">Farmaceutico -->
				
			<input type="radio" id="tipo" name="tipo" value="3" requiered>Representante
			<input type="radio" id="tipo" name="tipo" value="4" requiered>Gerente
		</article>

		<article>
			<label>Clave de Representante:<sup>(*)</sup></label>
			<input type="text" id="representante" name="representante" placeholder="Representante" required/>
		</article>

		<article>
			<label>Usuario:<sup>(*)</sup></label>
			<input type="text" id="user" name="user" placeholder="Escribe tu usuario" required/>
		</article>

		<article class="nota">
			<label>Contraseña:<sup>(*)</sup></label>
			<input type="password" id="pass" name="pass" placeholder="Escribe tu contraseña" required  />
			<p>(*)Campos obligatorios.</p>
		</article>

		<article class="enlace3">
			<input class="btn" type="submit" name="registrar" id="registrar" value="Registrar" />
			<a href="#login">Login</a>	
		</article>
		<a href="#close" class="close" ></a>

		 <?php
           					 switch ($error) {
               					  case -1:
               					     echo '';
               					     break;
               					 case -2:
               					     echo '<aside id="error_reg" class="error2"><p>Error al registrarse, el usuario ya existente.</p></aside>';
               					     break;
               					 case -3:
               					     echo '';
               					     break;
               					 default:
               					     if($registrar) echo '<aside id="error_reg" class="error4"><p>Se ha registrado correctamente.</p></aside>';
               					     break;
          						  }
						?>
	</form>

    </section>

	<section class="cont_index">
		<a href="#login"><img src="img/Caja_bienvenida.png" /></a>
	</section>

	<a href="#" class="vent_emergente" id="aviso_priv"></a>
		<section class="popup2">
				<h2>Política de Privacidad</h2>
				<p>La confidencialidad y debida protección de la información personal confiada a PROBIOMED es de máxima importancia. 
					PROBIOMED está comprometido a manejar sus datos personales de manera responsable y con apego a lo previsto por la Ley Federal 
					de Protección de Datos Personales en Posesión de los Particulares (en adelante la “Ley”) y demás normatividad aplicable.</p>
				<p>Para PROBIOMED resulta necesaria la recopilación de ciertos datos personales para llevar a cabo las actividades intrínsecas a su giro 
					comercial y mercantil. PROBIOMED tiene la obligación legal y social de cumplir con las medidas, legales y de seguridad suficientes para proteger 
					aquellos datos personales que haya recabado para las finalidades que en la presente política de privacidad serán descritas.</p>
				<p>Todo lo anterior se realiza con el objetivo de que usted tenga pleno control y decisión sobre sus datos personales. Por ello, le recomendamos que 
					lea atentamente la siguiente información.</p>
				<p>Usted tendrá disponible en todo momento esta política de privacidad en nuestro sitio web <b>www.probiomed.com.mx</b>.</p>
				<article class="aviso">			
					<aside class="aviso_listado">
						<ul>
							<li class="1">Datos del responsable</li>
							<li class="2">Datos Personales recabados y datos sensibles</li>
							<li class="3">Finalidad del Tratamiento de Datos</li>
							<li class="4">Transferencia  y remisión de datos</li>
							<li class="5">Ejercicio de derechos ARCO</li>
							<li class="6">Revocación del Consentimiento y limitación de uso o divulgación</li>
							<li class="7">Cambios a la Política de Privacidad</li>
							<li class="8">Comité de Privacidad</li>
						</ul>
					</aside>
					<aside class="aviso_cont">
						<fieldset class="aviso1">
							<legend><h4>Datos del responsable</h4></legend>
							<p>PROBIOMED es una sociedad constituida de conformidad con las leyes de México con domicilio en Av. Ejercito Nacional No. 499, Col. Granada, 
								delegación Miguel Hidalgo, México, Distrito Federal.</p>
						</fieldset>
						<fieldset class="aviso2">
							<legend><h4>Datos Personales recabados y datos sensibles</h4></legend>
							<p>Como parte de nuestra comunidad de médicos, podrá ser recabada y tratada cierta información susceptible de identificación personal sobre usted.
 							Entre dicha información se podrá incluir de manera enunciativa, más no limitativa, la siguiente:</p>
							<li> Datos de identificación: nombre completo, teléfono particular, celular y/o de trabajo y fecha de nacimiento.</li>
							<li> Datos profesionales: cédula profesional, y especialidad principal.</li>
							<p>Asimismo le informamos que para cumplir con las finalidades de nuestra relación no es necesario tratar ningún tipo de dato sensible por
								lo que no serán recabados.</p>
							<p>Para las finalidades señaladas en el presente aviso, podemos recabar sus datos personales de distintas formas: cuando usted nos los
							proporciona directamente; cuando visita nuestro sitio de Internet o utiliza nuestros servicios en línea, y cuando obtenemos información 
							a través de otras fuentes que están permitidas por la Ley.</p>
						</fieldset>
						<fieldset class="aviso3">
							<legend><h4>Finalidad del Tratamiento de Datos</h4></legend>
							<p>Sus datos personales podrán ser utilizados para las siguientes finalidades relacionadas con su integración a nuestra comunidad de médicos:</p>
								<ul>
									<li>Identificación.</li>
									<li>Investigaciones de mercado y promoción de productos</li>
									<li>Para informarle de nuestros avances, promociones, eventos y en general, mantener contacto con usted.</li>
									<li>Hacer consultas, investigaciones y revisiones en relación a sus quejas o reclamaciones.</li>
									<li>Para fines estadísticos.</li>
									<li>Intercambio de información científica y educativa.</li>
									<li>Participación en programas médicos, de conocimiento de enfermedades y estudios clínicos.</li>
									<li>Reportes a las autoridades sanitarias relativas a posibles experiencias adveras a medicamentos.</li>
								</ul>
							<p>Si usted no desea que PROBIOMED trate su información para fines publicitarios, promocionales y de investigaciones de mercado podrá revocar su 
							consentimiento comunicándose con nosotros al correo electrónico: <b>privacidad@probiomed.com.mx</b></p>
						</fieldset>
						<fieldset class="aviso4">
							<legend><h4>Transferencia  y Remisión de Datos</h4></legend>
							<p>Su información personal no será transferida sin su previo consentimiento. </p>
							<p>Los datos personales recabados por cualquier medio electrónico o impreso para efectos de sus procesos de producción, comercialización, investigación 
							de mercados y/o de promoción de sus productos, así como los derivados de relaciones contractuales y/o laborales, se tratarán en los términos de la Ley, 
							para su almacenamiento, identificación, administración, análisis, operación y/o divulgación. Dichos datos pueden ser remitidos a empresas subsidiarias y 
							terceros con relaciones contractuales con la compañía. Para ello, en el contrato correspondiente PROBIOMED incluirá una cláusula de que dichas empresas 
							subsidiarias y terceros otorgan el nivel de protección de datos personales requerido por la Ley.</p>
							<p>Asimismo, para cumplir la(s) finalidad(es) anteriormente descrita(s) u otras aquellas exigidas legalmente o por las autoridades competentes, 
							PROBIOMED podrá transferir a sus empresas subsidiarias y a las autoridades competentes cierta información recopilada de usted.
							En todo momento, PROBIOMED salvaguardará la confidencialidad de los datos y el procesamiento de estos de tal manera que su privacidad esté 
							protegida en términos de la Ley, garantizando el cumplimiento del presente aviso por la empresa y por aquellos terceros con quienes mantenga 
							una relación jurídica para la adecuada prestación de sus servicios.</p>
						</fieldset>
						<fieldset class="aviso5">
							<legend><h4>Ejercicio de derechos ARCO</h4></legend>
							<p>Con apego a lo estipulado por la Ley, usted, o su representante legal en su caso, podrá ejercer los derechos de acceso, rectificación, 
							cancelación y oposición (en lo sucesivo Derechos Arco) a través de la solicitud correspondiente, a la dirección de correo electrónica 
							<b>privacidad@probiomed.com.mx</b>donde personal especializado atenderá en tiempo y forma su solicitud.</p>
						</fieldset>
						<fieldset class="aviso6">
							<legend><h4>Revocación del Consentimiento y limitación de uso o divulgación</h4></legend>
							<p>En todo momento, usted podrá revocar el consentimiento que nos ha otorgado para el tratamiento de sus datos personales, siempre y 
							cuando no sea necesario para cumplir obligaciones derivadas de nuestra relación jurídica, a fin de que dejemos de hacer uso o divulgación 
							de los mismos. Igualmente usted  podrá limitar el uso o divulgación de sus datos personales. Para ello, es necesario que presente su petición
						en la siguiente dirección electrónica: <b>privacidad@probiomed.com.mx.</b></p>
						</fieldset>
						<fieldset class="aviso7">
							<legend><h4>Cambios a la Política de Privacidad</h4></legend>
							<p>Cualquier cambio o modificación al presente aviso podrá efectuarse por esta empresa en cualquier momento y se dará a conocer a través de su 
							portal <b>www.probiomed.com.mx</b> y/o a través de medios impresos de circulación nacional.</p>
						</fieldset>
						<fieldset class="aviso8">
							<legend><h4>Comité de Privacidad</h4></legend>
							<p>El Comité de Privacidad de Datos le proporcionará la atención necesaria para el ejercicio de sus derechos ARCO y además, velará por la 
							protección de sus datos personales al interior de la organización.</p>
							<ul>
								<li><b>Dirección:</b> Av. Ejercito Nacional No. 499, Col. Granada, delegación Miguel Hidalgo, México, Distrito Federal.</li>
								<li><b>Correo electrónico:</b> privacidad@probiomed.com.mx</li>
								<li><b>Teléfono:</b> 11 66 20 00</li>
								<li><b>Horario:</b> 9:00 a 18:00 horas</li>
							</ul>
						</fieldset>
					</aside>
				</article>
				<a class="close" href="#close"></a>
	 	</section>	
	
	<footer>
        <aside id="aviso">
            <a href="#aviso_priv" id="aviso" class="cambio">Aviso de Privacidad </a>
            <a href="registrosssa-ipps.php" id="registros" class="cambio">Registros SSA e IPP'S </a>
        </aside>
		<aside id="derechos">2013 Todos los derechos reservados, Probiomed, SA de CV</aside>
		<aside id="compatibilidad">Compatibilidad con G. Chrome, Firefox, Safari e I. Explorer 10+</aside>
	</footer>
</body>
</html>