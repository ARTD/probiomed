﻿<?php
//Revisa el usuario o las cookies, si no existen no permite entrar
require ("funciones.php");
include ("conexion.php");
$error = 0;

 seguridad();//comprobamos que se esté logueado

if(isset($_POST['salir']))
{    
    
    destruirCookie($_COOKIE['identificado']);
    
    $_SESSION = array();
 
    //guardar el nombre de la sessión para luego borrar las cookies
    $session_name = session_name();
 
    //Para destruir una variable en específico
    unset($_SESSION['usuarios']);
 
    // Finalmente, destruye la sesión
    session_destroy();
 
    // Para borrar las cookies asociadas a la sesión
    // Es necesario hacer una petición http para que el navegador las elimine
    if ( isset( $_COOKIE[ $session_name ] ) ) {
        setcookie($session_name, '', time()-3600, '/');   
    }
    if(isset($_COOKIE['identificado'])){
        setcookie('identificado', '', time()-3600, '/'); 
       
    }
    header("Location: index.php");
    exit();
   
}

$tipousuario=isset($_SESSION['tipousuario']) ? $_SESSION['tipousuario'] : NULL;
$user=isset($_SESSION['usuarios']) ? $_SESSION['usuarios'] : NULL;
$representante=isset($_SESSION['representante']) ? $_SESSION['representante'] : NULL;


if(!isset($tipousuario))
{
	//Saber valor del tipo de usuario
	$cook=$_COOKIE["identificado"];
	$sql = "select * from usuarios where cookie='".$cook."'";
	$result = mysql_query($sql,$conexion);
	$row = mysql_fetch_row($result);
	$tipousuario = $row[7];
	$user = $row[0];
    
    $sql1 = "select B.representante from usuarios A, datos B where A.id_usuarios= 1 and A.id_datos = B.id_datos";
    $result1 = mysql_query($sql1,$conexion);
    $row1 = mysql_fetch_row($result1);
    $representante = $row1[0];
    
} 

if($tipousuario != 4)
{
	header("Location: contenido.php");
}


?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="iso-8859-1" />
	<meta name="description" content"Sitio exclusivo para Probiomed" /><!--Descripción general del sitio-->
	<link type="image/x-icon" href="img/favicon.ico" rel="icon"/>
	<link rel="sitemap" type="application/xml" title="Sitemap" href="sitemap.xml" />
	<link rel="stylesheet" href="css/estilos.css"/><!--Referencia a la Hoja de Estilos-->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script> <!--Referencia a Script conexión web-->
	<script> 
		!window.jQuery && document.write("<script src='js/jquery.min.js'><\/script>");
	</script><!--Referencia a Script conexión local-->
	<script src="js/efectos.js"></script> <!--Referencia al JavaScript "Efectos"-->
</head>
<body>
	<header id="cabecera"> <!--Encabezado-->
		<img src="img/logo_pr.png"> <!--Logo Probiomed Rewards-->
		<article class="bt_cabecera_rep" >
			<form name="login" method="post" action="">
				<input type="submit" name="salir" id="salir" value="Cerrar Sesión"/>
			</form>
		</article>
	</header>
	<section id="contenido"><!--Contendedor principal-->
		<section class="reporte"> <!--Sección de Reporte-->
		<h1>Gerentes Marca / División</h1>
			<fieldset>
				<legend>
					<?php
					include('conexion.php');

					$sql = "SELECT usuario FROM usuarios WHERE id_tipo = 1 OR id_tipo = 2";
					// Se hace la llamada a la base de datos
					$result = mysqli_query($con,$sql);

					// Si no hary resultado exitoso se despliega un error en pantalla
					if(!$result) {
					echo "No Hay resultado";
					}
					// De lo contrario se hace un deplegado de una tabla con los datos necesarios
					else {
					$row = mysqli_num_rows($result);
					echo "Número de registros al programa en general: ".$row;
					}
					mysqli_close($con);
					?>
				</legend>
				<article>
					<aside class="subtitulo">
						Reporte General por clave de representante: 
					</aside>
					<form>
					<select name="users" onchange="showUser(this.value)">
					<option value="">Seleccione la clave:</option>
						<?php
						include('conexion.php');
						// Se hace el reporte de datos deseado
						$sql = "SELECT * FROM representante";
						// Se hace la llamada a la base de datos
						$result = mysqli_query($con,$sql);
						// Se navega en todo el array
						while ($rows = mysqli_fetch_array($result))
						{?>
						<!-- Aqui se le indica a la computadora que tiene que imprimir los resultados del "id" en el campo "value" 
						Y los resultados del campo "especialidad" serán puestos como el nombre seleccionable-->
						<option value="<?php echo $rows['id_representante'];?>"><?php echo $rows['codigo'];?></option>
						<?php
						}?>
					</select>
					</form>
					<aside class="impresion">
						<a href="ex.php"><img src="img/impresion.png"/>Versión Imprimible</a>
					</aside>
						<div id="total"></div>
						<div id="txtHint"><b></b></div>
				</article>
				<article>
					<h3>Registro de Ganadores</h3>
					<aside class="subtitulo">
						Reporte de Ganadores médicos:
					</aside>
					
					<aside class="impresion">
						<a  href="ex2.php"><img src="img/impresion.png"/>Versión Imprimible</a>
					</aside>
					<div id="totalGana"></div>
					<div id="reporteGana"><b></b></div>
					<!-- Sección de farmaceuticos -->
					<article>
						<aside class="subtitulo2">
							Reporte de Ganadores farmacéuticos:
							<aside class="impresion3">
								<a href="ex4.php"><img src="img/impresion.png"/>Versión Imprimible</a>
							</aside>
						</aside>
					</article>
					
					<?php
					  include('conexion.php');
					  $sql="SELECT * FROM ganadoresfarma";

					  $result = mysqli_query($con,$sql);  

					  echo "<table id='ganadoresfarma'>
					  <tr>
					  <th>Nombre</th>
					  <th>Apellido Paterno</th>
					  <th>Apellido Materno</th>  
					  <th>Puntos</th>
					  <th>Lugar</th>
					  <th>Mes</th>
					  </tr>";

					  while($row = mysqli_fetch_array($result))
					    {
					    echo "<tr>";
					    echo "<td>" . $row['nombre'] . "</td>";
					    echo "<td>" . $row['apellido_pa'] . "</td>";
					    echo "<td>" . $row['apellido_ma'] . "</td>";
					    echo "<td>" . $row['puntos'] . "</td>";
					    echo "<td>" . $row['lugar'] . "</td>";
					    echo "<td>" . $row['mes'] . "</td>";
					    echo "</tr>";
					    }
					  echo "</table>";

					  $row = mysqli_num_rows($result);
					  echo "Numero de registros de ganadores farmacéuticos: ".$row;
					  ?>
					<div id="totalGana"></div>
					<div id="reporteGana"><b></b></div>
				</article>








				<article>
					<h3>Reporte de Juegos</h3>
					<aside class="tabla_gerente">
					<?php
						include('conexion.php');
						$sql = "SELECT nombre_juego,veces_jugadas FROM juegos ORDER BY veces_jugadas DESC";
						// Se hace la llamada a la base de datos
						$result = mysqli_query($con,$sql);

						// Si no hary resultado exitoso se despliega un error en pantalla
						if(!$result) {
							echo "No Hay resultado";
						}
						// De lo contrario se hace un deplegado de una tabla con los datos necesarios
						else {
							echo "<table border='1'>
							<tr>
							<th>Nombre</th>
							<th>Veces Jugadas</th>
							</tr>";

							while($row = mysqli_fetch_array($result)) {
							echo "<tr>";
							echo "<td>" . $row['nombre_juego'] . "</td>";
							echo "<td>" . $row['veces_jugadas'] . "</td>";
							echo "</tr>";
							}
						echo "</table>";
						}
						mysqli_close($con);
						?>
						</aside>
				</article>
				<article>
					<h3>Comentarios y Preguntas</h3>
					<aside class="tabla_gerente">
				<?php
					include('conexion.php');			
					$sql = "SELECT usuario,comentario,fecha FROM comentarios";
					// Se hace la llamada a la base de datos
					$result = mysqli_query($con,$sql);
			
					// Si no hary resultado exitoso se despliega un error en pantalla
					if(!$result) {
						echo "No Hay resultado";
					}
					// De lo contrario se hace un deplegado de una tabla con los datos necesarios
					else {
						echo "<table border='1'>
						<tr>
						<th>Usuario</th>
						<th>Comentario</th>
						<th>Fecha</th>
						</tr>";

					while($row = mysqli_fetch_array($result)) {
						echo "<tr>";
						echo "<td>" . $row['usuario'] . "</td>";
						echo "<td>" . $row['comentario'] . "</td>";
						echo "<td>" . $row['fecha'] . "</td>";
						echo "</tr>";
						}
					echo "</table>";
					}
					mysqli_close($con);
					?>
					</aside>
				</article>
			</fieldset>
		</section>
	</section>


	<a href="#" class="vent_emergente" id="aviso_priv"></a>
		<section class="popup2">
				<h2>Política de Privacidad</h2>
				<p>La confidencialidad y debida protección de la información personal confiada a PROBIOMED® es de máxima importancia. 
					PROBIOMED® está comprometido a manejar sus datos personales de manera responsable y con apego a lo previsto por la Ley Federal 
					de Protección de Datos Personales en Posesión de los Particulares (en adelante la “Ley”) y demás normatividad aplicable.</p>
				<p>Para PROBIOMED® resulta necesaria la recopilación de ciertos datos personales para llevar a cabo las actividades intrínsecas a su giro 
					comercial y mercantil. PROBIOMED® tiene la obligación legal y social de cumplir con las medidas, legales y de seguridad suficientes para proteger 
					aquellos datos personales que haya recabado para las finalidades que en la presente política de privacidad serán descritas.</p>
				<p>Todo lo anterior se realiza con el objetivo de que usted tenga pleno control y decisión sobre sus datos personales. Por ello, le recomendamos que 
					lea atentamente la siguiente información.</p>
				<p>Usted tendrá disponible en todo momento esta política de privacidad en nuestro sitio web <b>www.probiomed.com.mx</b>.</p>
				<article class="aviso">			
					<aside class="aviso_listado">
						<ul>
							<li class="1">Datos del responsable</li>
							<li class="2">Datos Personales recabados y datos sensibles</li>
							<li class="3">Finalidad del Tratamiento de Datos</li>
							<li class="4">Transferencia  y remisión de datos</li>
							<li class="5">Ejercicio de derechos ARCO</li>
							<li class="6">Revocación del Consentimiento y limitación de uso o divulgación</li>
							<li class="7">Cambios a la Política de Privacidad</li>
							<li class="8">Comité de Privacidad</li>
						</ul>
					</aside>
					<aside class="aviso_cont">
						<fieldset class="aviso1">
							<legend><h4>Datos del responsable</h4></legend>
							<p>PROBIOMED® es una sociedad constituida de conformidad con las leyes de México con domicilio en Av. Ejercito Nacional No. 499, Col. Granada, 
								delegación Miguel Hidalgo, México, Distrito Federal.</p>
						</fieldset>
						<fieldset class="aviso2">
							<legend><h4>Datos Personales recabados y datos sensibles</h4></legend>
							<p>Como parte de nuestra comunidad de médicos, podrá ser recabada y tratada cierta información susceptible de identificación personal sobre usted.
 							Entre dicha información se podrá incluir de manera enunciativa, más no limitativa, la siguiente:</p>
							<li> Datos de identificación: nombre completo, teléfono particular, celular y/o de trabajo y fecha de nacimiento.</li>
							<li> Datos profesionales: cédula profesional, y especialidad principal.</li>
							<p>Asimismo le informamos que para cumplir con las finalidades de nuestra relación no es necesario tratar ningún tipo de dato sensible por
								lo que no serán recabados.</p>
							<p>Para las finalidades señaladas en el presente aviso, podemos recabar sus datos personales de distintas formas: cuando usted nos los
							proporciona directamente; cuando visita nuestro sitio de Internet o utiliza nuestros servicios en línea, y cuando obtenemos información 
							a través de otras fuentes que están permitidas por la Ley.</p>
						</fieldset>
						<fieldset class="aviso3">
							<legend><h4>Finalidad del Tratamiento de Datos</h4></legend>
							<p>Sus datos personales podrán ser utilizados para las siguientes finalidades relacionadas con su integración a nuestra comunidad de médicos:</p>
								<ul>
									<li>Identificación.</li>
									<li>Investigaciones de mercado y promoción de productos</li>
									<li>Para informarle de nuestros avances, promociones, eventos y en general, mantener contacto con usted.</li>
									<li>Hacer consultas, investigaciones y revisiones en relación a sus quejas o reclamaciones.</li>
									<li>Para fines estadísticos.</li>
									<li>Intercambio de información científica y educativa.</li>
									<li>Participación en programas médicos, de conocimiento de enfermedades y estudios clínicos.</li>
									<li>Reportes a las autoridades sanitarias relativas a posibles experiencias adveras a medicamentos.</li>
								</ul>
							<p>Si usted no desea que PROBIOMED® trate su información para fines publicitarios, promocionales y de investigaciones de mercado podrá revocar su 
							consentimiento comunicándose con nosotros al correo electrónico: <b>privacidad@probiomed.com.mx</b></p>
						</fieldset>
						<fieldset class="aviso4">
							<legend><h4>Transferencia  y remisión de datos</h4></legend>
							<p>Su información personal no será transferida sin su previo consentimiento. </p>
							<p>Los datos personales recabados por cualquier medio electrónico o impreso para efectos de sus procesos de producción, comercialización, investigación 
							de mercados y/o de promoción de sus productos, así como los derivados de relaciones contractuales y/o laborales, se tratarán en los términos de la Ley, 
							para su almacenamiento, identificación, administración, análisis, operación y/o divulgación. Dichos datos pueden ser remitidos a empresas subsidiarias y 
							terceros con relaciones contractuales con la compañía. Para ello, en el contrato correspondiente PROBIOMED® incluirá una cláusula de que dichas empresas 
							subsidiarias y terceros otorgan el nivel de protección de datos personales requerido por la Ley.</p>
							<p>Asimismo, para cumplir la(s) finalidad(es) anteriormente descrita(s) u otras aquellas exigidas legalmente o por las autoridades competentes, 
							PROBIOMED® podrá transferir a sus empresas subsidiarias y a las autoridades competentes cierta información recopilada de usted.
							En todo momento, PROBIOMED® salvaguardará la confidencialidad de los datos y el procesamiento de estos de tal manera que su privacidad esté 
							protegida en términos de la Ley, garantizando el cumplimiento del presente aviso por la empresa y por aquellos terceros con quienes mantenga 
							una relación jurídica para la adecuada prestación de sus servicios.</p>
						</fieldset>
						<fieldset class="aviso5">
							<legend><h4>Ejercicio de derechos ARCO</h4></legend>
							<p>Con apego a lo estipulado por la Ley, usted, o su representante legal en su caso, podrá ejercer los derechos de acceso, rectificación, 
							cancelación y oposición (en lo sucesivo Derechos Arco) a través de la solicitud correspondiente, a la dirección de correo electrónica 
							<b>privacidad@probiomed.com.mx</b>donde personal especializado atenderá en tiempo y forma su solicitud.</p>
						</fieldset>
						<fieldset class="aviso6">
							<legend><h4>Revocación del Consentimiento y limitación de uso o divulgación</h4></legend>
							<p>En todo momento, usted podrá revocar el consentimiento que nos ha otorgado para el tratamiento de sus datos personales, siempre y 
							cuando no sea necesario para cumplir obligaciones derivadas de nuestra relación jurídica, a fin de que dejemos de hacer uso o divulgación 
							de los mismos. Igualmente usted  podrá limitar el uso o divulgación de sus datos personales. Para ello, es necesario que presente su petición
						en la siguiente dirección electrónica: <b>privacidad@probiomed.com.mx.</b></p>
						</fieldset>
						<fieldset class="aviso7">
							<legend><h4>Cambios a la Política de Privacidad</h4></legend>
							<p>Cualquier cambio o modificación al presente aviso podrá efectuarse por esta empresa en cualquier momento y se dará a conocer a través de su 
							portal <b>www.probiomed.com.mx</b> y/o a través de medios impresos de circulación nacional.</p>
						</fieldset>
						<fieldset class="aviso8">
							<legend><h4>Comité de Privacidad</h4></legend>
							<p>El Comité de Privacidad de Datos le proporcionará la atención necesaria para el ejercicio de sus derechos ARCO y además, velará por la 
							protección de sus datos personales al interior de la organización.</p>
							<ul>
								<li><b>Dirección:</b> Av. Ejercito Nacional No. 499, Col. Granada, delegación Miguel Hidalgo, México, Distrito Federal.</li>
								<li><b>Correo electrónico:</b> privacidad@probiomed.com.mx</li>
								<li><b>Teléfono:</b> 11 66 20 00</li>
								<li><b>Horario:</b> 9:00 a 18:00 horas</li>
							</ul>
						</fieldset>
					</aside>
				</article>
				<a class="close" href="#close"></a>
	 	</section>	
	<footer>
        <aside id="aviso">
            <a href="#aviso_priv" id="aviso" class="cambio">Aviso de Privacidad </a>
            <a href="registrosssa-ipps.php" id="registros" class="cambio">Registros SSA e IPP'S </a>
        </aside>
		<aside id="compatibilidad">Compatibilidad con: <img src="img/navegadores.png"><img class="marca" src="img/probiomed.png"> </aside> 
		<aside id="derechos">® 2013 Todos los derechos reservados, Probiomed ®, SA de CV</aside>
	</footer>
</body>
</html>