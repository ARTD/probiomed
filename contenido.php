<!DOCTYPE html>

<?php

require ("funciones.php");
include "conexion.php";

$error = 0;



seguridad(); //comprobamos que se esté logueado

if(isset($_POST['salir']))
{

    destruirCookie($_COOKIE['identificado']);

    $_SESSION = array();

    //guardar el nombre de la sessión para luego borrar las cookies
    $session_name = session_name();

    //Para destruir una variable en específico
    unset($_SESSION['usuarios']);

    // Finalmente, destruye la sesión
    session_destroy();

    // Para borrar las cookies asociadas a la sesión
    // Es necesario hacer una petición http para que el navegador las elimine
    if ( isset( $_COOKIE[ $session_name ] ) ) {
        setcookie($session_name, '', time()-604800, '/');
    }
    if(isset($_COOKIE['identificado'])){
        setcookie('identificado', '', time()-604800, '/');
    }
    header("Location: index.php");
    exit();

}

$tipousuario=isset($_SESSION['tipousuario']) ? $_SESSION['tipousuario'] : NULL;
$user=isset($_SESSION['usuarios']) ? $_SESSION['usuarios'] : NULL;

if(!isset($tipousuario))
{
	//Saber valor del tipo de usuario
	$cook=$_COOKIE["identificado"];
	$sql = "select * from usuarios where cookie='".$cook."'";
	$result = mysqli_query($conexion,$sql);
	$row = mysqli_fetch_row($result);
	$tipousuario = $row[7];
	$user = $row[0];

}
?>

<html lang="es">
<head>
	<meta charset="utf-8" />
	<title>Probiomed</title><!--Título del sitio-->
	<meta name="description" content="Sitio exclusivo para Probiomed" /><!--Descripción general del sitio-->
	<link type="image/x-icon" href="img/favicon.ico" rel="icon"/>
	<link rel="sitemap" type="application/xml" title="Sitemap" href="sitemap.xml" />
	<link rel="stylesheet" href="css/estilos.css"/><!--Referencia a la Hoja de Estilos-->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script> <!--Referencia a Script conexión web-->
	<script> 
		!window.jQuery && document.write("<script src='js/jquery.min.js'><\/script>");
	</script><!--Referencia a Script conexión local-->
	<script src="js/efectos.js"></script> <!--Referencia al JavaScript "Efectos"-->
</head>
<body> <!--Contenido-->

	<?php /*  echo "
                <script>
                alert('Tipo Usuario: ".$tipousuario ."');
                </script>"; */ ?>
	<header id="cabecera"> <!--Encabezado-->
		<img src="img/logo_pr.png"> <!--Logo Probiomed Rewards-->
		<article class="bt_cabecera" >
		<form name="login" method="post" action="">
			<?php

		    	$sql = "SELECT usuario,puntos FROM usuarios WHERE id_usuarios = '$user'";
		    	$result = mysqli_query($con,$sql);
				// Se navega en todo el array
				while ($rows = mysqli_fetch_array($result))
				{
						$u = $rows['usuario'];
					?>
					<!-- Aqui se le indica a la computadora que tiene que imprimir los resultados del "id" en el campo "value"
					Y los resultados del campo "especialidad" serán puestos como el nombre seleccionable-->
					<p class="puntaje"> Usuario: <?php echo $rows['usuario'];?> </p>
					<p class="puntaje2"> Puntos: <?php echo utf8_encode($rows['puntos']);?> </p>

				<?php



			}?>

			<?php if($tipousuario == 3) header ("Location: representante.php");
					else if($tipousuario == 4) header ("Location: gerente.php");

			?>

			<input type="submit" name="salir" id="salir" value="Cerrar Sesión"/>
			<a class="comentario" href="#comentario"> Comentarios</a>
		</form>
		</article> 
	</header>
	<nav> <!--Menú-->
		<ul>
			<li><a href="#" id="uno"><img src="img/dinamicas.png"></a></li>
			<li><a href="#" id="dos"><img src="img/recompensas.png"></a></li>
			<li><a href="#" id="tres"><img src="img/topten_full.png"></a></li>
		</ul>
	</nav>
	<!--Ventana Emergente de Comentarios-->
	<a href="#" class="vent_emergente" id="comentario"></a>
	<section class="popup6">
		<form name="login" method="post" <?php echo "action='comentario.php?u=".$u."'"; ?> >
				<h2>Comentarios y/o Sugerencias</h2>
					<article>
						<textarea id="coment" name="coment"  placeholder="Escribe tus comentarios y/o sugerencias." requiered> </textarea>
					</article>
					<aside class="enlace2">
						<input class="btn" type="submit" name="comentario" id="comentario" value="Enviar" />
					</aside>
					<a class="close" href="#close"></a>
		</form>
	 </section>


	 <?php 

	 	if(isset($_GET["c"])){
            $c = $_GET["c"];

            if($c==1) {
                echo "<script>window.alert('Tu comentario ha sido guardado.Gracias');</script>";
            }
        }

	  ?>
	<section id="contenido"><!--Contendedor principal-->
		<section class="dinamicas"> <!--Sección de Dinámicas-->
				<a href="juegos/4-en-linea/index.php" target="_self"><img src="img/graficos_juegos/juego1.png" title="Consigue 4 imágenes iguales y obtendrás puntos."/></a><!--Juego 1-->
      			<a href="juegos/CanvasPuzzle/index.php" target="_self"><img src="img/graficos_juegos/juego2.png" title="Logra armar el rompecabezas en el menor tiempo posible."/></a><!--Juego 2-->
      			<a href="juegos/memoria/index.php" target="_self" ><img src="img/graficos_juegos/juego3.png" title="Prueba tu habilidad mental, consiguiendo la mayor cantidad de pares, en el menor tiempo posible. "/></a><!--Juego 3-->
				<a href="juegos/SopaDeLetras/index.php" target="_self"><img src="img/graficos_juegos/juego4.png" title="Prueba tu habilidad para descubrir palabras ocultas."/></a><!--Juego 4-->
      			<a href="juegos/Toma_el_control/index.html" target="_blank"><img src="img/graficos_juegos/juego5.png" title="Alimenta a nuestro amigo sanamente."/></a><!--Juego 5-->
      			<a href="juegos/wordinvaders/index.php" target="_self"><img src="img/graficos_juegos/juego6n.png" title="¡Eres nuestra única esperanza! Salva al mundo destruyendo a los invasores extraterrestres."/></a><!--Juego 6-->
      			<a href="juegos/Salva_al_paciente/index.php" target="_self"><img src="img/graficos_juegos/juego7.png" title="Nuestro paciente está enfermo, ayúdalo a sanar."/></a><!--Juego 7-->
		</section>

		<!--Médicos-->
		<?php if($tipousuario == 1) { ?>

		<section id="recompensas_m" class="recompensas"> <!--Sección de Recompensas-->
			<section class="gal_recom"><!--Premios de Ganadores-->
				<section><!--Fotografía de Premios-->
				<article class="cont_recom3">
					<aside class="premios2"><img src="img/recompensas/medicos/img2.jpg"></aside>
					<img class="galardon" src="img/galardon2.png">
				</article>
      			<article class="cont_recom3">
					<aside class="premios1"><img src="img/recompensas/medicos/img1.JPG"></aside>
					<img class="galardon" src="img/galardon1.png">
				</article>
				<article class="cont_recom3">
					<aside class="premios3"><img src="img/recompensas/medicos/img3.jpg"></aside>
					<img class="galardon" src="img/galardon3.png">
				</article>
				</section>
			</section>

		<div href="#" class="mecanica">
			<img class="mec_juegos" src="img/mecanica_premios.png">
			<div class="mecanica_text">
				<p>Mensualmente los 3 participantes que más puntos acumulen podrán ganar los siguientes premios*:</p>
				<lu>
					<li>1er lugar: I-pad</li>
					<li>2do Lugar: I-pad mini</li>
					<li>3er Lugar: I-Pod nano</li>
				</lu>
				<p>Además, los participantes que ocupen las posiciones del  4 al 10 podrán obtener: ESTETOSCOPIO LITTMANN CLASSIC II ADULTO BLACK.</p>
				<p>Al inicio de cada mes, todos los participantes empiezan nuevamente desde cero, de esta forma todos tienen la misma posibilidad de obtener 
				   los premios mensuales.</p>
				<p>Sin embargo, se llevará el acumulado de cada participante y en el semestre, quien más puntos acumule podrá llevarse una BECA todo incluido, 
				   para asistir a un CONGRESO NACIONAL.</p>
				<p>*Los premios pueden variar mensualmente, en la primera semana de cada mes se establecerán cuáles son los premios que pueden obtener.</p>
				<p>La marca registrada "PROBIOMED®" y cualquier otro producto, servicio, submarca o logos de  "PROBIOMED®"  usados, citados y/o referenciados en este Sitio Web son marcas registradas de  "PROBIOMED S.A. de C.V." Cualquier otra marca registrada utilizada en esta página es propiedad de sus respectivos dueños. Otros productos y/o nombres de Compañías usados en este Sitio Electrónico pueden ser protegidos por sus respectivas marcas registradas o patentes. El contenido de este sitio no debe ser interpretado como permiso otorgado, implícito, o de alguna otra forma, licencia o derecho al uso de cualquier marca registrada exhibida en la página, sin el permiso por escrito de Probiomed o de su respectivo dueño.</p>
			</div>
		</div>

		</section> <?php } ?>

	<!--Farmaceuticos-->
	<?php if($tipousuario == 2) { ?>

		<section id="recompensas_f" class="recompensas"> <!--Sección de Recompensas-->
			<section class="gal_recom"><!--Premios de Ganadores-->
				<section><!--Fotografía de Premios-->
				<article class="cont_recom3">
					<aside class="premios2"><img src="img/recompensas/farmaceuticos/img2.jpg"></aside>
				</article>
      			<article class="cont_recom3">
					<aside class="premios1"><img src="img/recompensas/farmaceuticos/img1.JPG"></aside>
				</article>
				<article class="cont_recom3">
					<aside class="premios3"><img src="img/recompensas/farmaceuticos/img3.jpg"></aside>
				</article>
				</section>
			</section>
			<div href="#" class="mecanica">
			<img class="mec_juegos" src="img/mecanica_premios.png">
			<div class="mecanica_text">
				<p>Mensualmente los 3 participantes que más puntos acumulen podrán ganar los siguientes premios*:</p>
				<lu>
					<li>1er lugar: I-pad</li>
					<li>2do Lugar: I-pad mini</li>
					<li>3er Lugar: I-Pod nano</li>
				</lu>
				<p>Además, los participantes que ocupen las posiciones del  4 al 10 podrán obtener: ESTETOSCOPIO LITTMANN CLASSIC II ADULTO BLACK.</p>
				<p>Al inicio de cada mes, todos los participantes empiezan nuevamente desde cero, de esta forma todos tienen la misma posibilidad de obtener 
				   los premios mensuales.</p>
				<p>Sin embargo, se llevará el acumulado de cada participante y en el semestre, quien más puntos acumule podrá llevarse una BECA todo incluido, 
				   para asistir a un CONGRESO NACIONAL.</p>
				<p>*Los premios pueden variar mensualmente, en la primera semana de cada mes se establecerán cuáles son los premios que pueden obtener.</p>
			</div>
		</div>
		</section> <?php } ?>
	
		<!--Médicos-->

		<?php if($tipousuario == 1) { ?>

		<section id="topten_m" class="topten"> <!--Sección de Top Ten - Ganador del mes-->
				<article class="meses">
					<input onclick="historialGanaMedico(this.name)" type="submit" name="January" id="enero" value="Enero"/>
					<input onclick="historialGanaMedico(this.name)" type="submit" name="February" id="febrero" value="Febrero"/>	
					<input onclick="historialGanaMedico(this.name)" type="submit" name="March" id="marzo" value="Marzo"/>
					<input onclick="historialGanaMedico(this.name)" type="submit" name="April" id="abril" value="Abril"/>
					<input onclick="historialGanaMedico(this.name)" type="submit" name="May" id="mayo" value="Mayo"/>
					<input onclick="historialGanaMedico(this.name)" type="submit" name="June" id="junio" value="Junio"/>
					<input onclick="historialGanaMedico(this.name)" type="submit" name="July" id="julio" value="Julio"/>
					<input onclick="historialGanaMedico(this.name)" type="submit" name="August" id="agosto" value="Agosto"/>
					<input onclick="historialGanaMedico(this.name)" type="submit" name="September" id="sept" value="Septiembre"/>
					<input onclick="historialGanaMedico(this.name)" type="submit" name="October" id="oct" value="Octubre"/>
					<input onclick="historialGanaMedico(this.name)" type="submit" name="November" id="nov" value="Noviembre"/>
					<input onclick="historialGanaMedico(this.name)" type="submit" name="December" id="dic" value="Diciembre"/>
				</article>
				<article class="cont_mes" id="mesganamedico">
					<aside class="mes" ><p>Mes</p></aside>
						<p id="lugarganamedico">Lugar: 1° Usuario:</p>
						<p id="lugarganamedico">Lugar: 2° Usuario:</p>
						<p id="lugarganamedico">Lugar: 3° Usuario:</p>
						<p id="lugarganamedico">Lugar: 4° Usuario:</p>
						<p id="lugarganamedico">Lugar: 5° Usuario:</p>
						<p id="lugarganamedico">Lugar: 6° Usuario:</p>
						<p id="lugarganamedico">Lugar: 7° Usuario:</p>
						<p id="lugarganamedico">Lugar: 8° Usuario:</p>
						<p id="lugarganamedico">Lugar: 9° Usuario:</p>
						<p id="lugarganamedico">Lugar: 10° Usuario:</p>					
				</article>
		</section>	 <?php } ?>

		<?php if($tipousuario == 2) { ?>

		<!--Farmaceuticos-->
		<section id="topten_f" class="topten"> <!--Sección de Top Ten - Ganador del mes-->
			<article class="meses" >
				<input onclick="historialGanaFarma(this.name)" type="submit" name="January" id="enero" value="Enero"/>
				<input onclick="historialGanaFarma(this.name)" type="submit" name="February" id="febrero" value="Febrero"/>	
				<input onclick="historialGanaFarma(this.name)" type="submit" name="March" id="marzo" value="Marzo"/>
				<input onclick="historialGanaFarma(this.name)" type="submit" name="April" id="abril" value="Abril"/>
				<input onclick="historialGanaFarma(this.name)" type="submit" name="May" id="mayo" value="Mayo"/>
				<input onclick="historialGanaFarma(this.name)" type="submit" name="June" id="junio" value="Junio"/>
				<input onclick="historialGanaFarma(this.name)" type="submit" name="July" id="julio" value="Julio"/>
				<input onclick="historialGanaFarma(this.name)" type="submit" name="August" id="agosto" value="Agosto"/>
				<input onclick="historialGanaFarma(this.name)" type="submit" name="September" id="sept" value="Septiembre"/>
				<input onclick="historialGanaFarma(this.name)" type="submit" name="October" id="oct" value="Octubre"/>
				<input onclick="historialGanaFarma(this.name)" type="submit" name="November" id="nov" value="Noviembre"/>
				<input onclick="historialGanaFarma(this.name)" type="submit" name="December" id="dic" value="Diciembre"/>
			</article>
			<article class="cont_mes" id="mesganamedico">
				<aside class="mes" ><p>Mes</p></aside>
						<p id="lugarganamedico">Lugar: 1° Usuario:</p>
						<p id="lugarganamedico">Lugar: 2° Usuario:</p>
						<p id="lugarganamedico">Lugar: 3° Usuario:</p>
						<p id="lugarganamedico">Lugar: 4° Usuario:</p>
						<p id="lugarganamedico">Lugar: 5° Usuario:</p>
						<p id="lugarganamedico">Lugar: 6° Usuario:</p>
						<p id="lugarganamedico">Lugar: 7° Usuario:</p>
						<p id="lugarganamedico">Lugar: 8° Usuario:</p>
						<p id="lugarganamedico">Lugar: 9° Usuario:</p>
						<p id="lugarganamedico">Lugar: 10° Usuario:</p>
			</article>
		</section>	<?php } ?>
			
		<!-- Aviso de Privacidad --> 
		
		<a href="#" class="vent_emergente" id="aviso_priv"></a>
		<section class="popup2">
				<h2>Política de Privacidad</h2>
				<p>La confidencialidad y debida protección de la información personal confiada a PROBIOMED® es de máxima importancia. 
					PROBIOMED® está comprometido a manejar sus datos personales de manera responsable y con apego a lo previsto por la Ley Federal 
					de Protección de Datos Personales en Posesión de los Particulares (en adelante la “Ley”) y demás normatividad aplicable.</p>
				<p>Para PROBIOMED® resulta necesaria la recopilación de ciertos datos personales para llevar a cabo las actividades intrínsecas a su giro 
					comercial y mercantil. PROBIOMED® tiene la obligación legal y social de cumplir con las medidas, legales y de seguridad suficientes para proteger 
					aquellos datos personales que haya recabado para las finalidades que en la presente política de privacidad serán descritas.</p>
				<p>Todo lo anterior se realiza con el objetivo de que usted tenga pleno control y decisión sobre sus datos personales. Por ello, le recomendamos que 
					lea atentamente la siguiente información.</p>
				<p>Usted tendrá disponible en todo momento esta política de privacidad en nuestro sitio web <b>www.probiomed.com.mx</b>.</p>
				<article class="aviso">			
					<aside class="aviso_listado">
						<ul>
							<li class="1">Datos del responsable</li>
							<li class="2">Datos Personales recabados y datos sensibles</li>
							<li class="3">Finalidad del Tratamiento de Datos</li>
							<li class="4">Transferencia  y remisión de datos</li>
							<li class="5">Ejercicio de derechos ARCO</li>
							<li class="6">Revocación del Consentimiento y limitación de uso o divulgación</li>
							<li class="7">Cambios a la Política de Privacidad</li>
							<li class="8">Comité de Privacidad</li>
						</ul>
					</aside>
					<aside class="aviso_cont">
						<fieldset class="aviso1">
							<legend><h4>Datos del responsable</h4></legend>
							<p>PROBIOMED® es una sociedad constituida de conformidad con las leyes de México con domicilio en San Esteban No. 88 Col. Santo Tomás, 
							   Del. Azcapotzalco, México DF, C.P. 02020</p>
						</fieldset>
						<fieldset class="aviso2">
							<legend><h4>Datos Personales recabados y datos sensibles</h4></legend>
							<p>Como parte de nuestra comunidad de médicos, podrá ser recabada y tratada cierta información susceptible de identificación personal sobre usted.
 							Entre dicha información se podrá incluir de manera enunciativa, más no limitativa, la siguiente:</p>
							<li> Datos de identificación: nombre completo, teléfono particular, celular y/o de trabajo y fecha de nacimiento.</li>
							<li> Datos profesionales: cédula profesional, y especialidad principal.</li>
							<p>Asimismo le informamos que para cumplir con las finalidades de nuestra relación no es necesario tratar ningún tipo de dato sensible por
								lo que no serán recabados.</p>
							<p>Para las finalidades señaladas en el presente aviso, podemos recabar sus datos personales de distintas formas: cuando usted nos los
							proporciona directamente; cuando visita nuestro sitio de Internet o utiliza nuestros servicios en línea, y cuando obtenemos información 
							a través de otras fuentes que están permitidas por la Ley.</p>
						</fieldset>
						<fieldset class="aviso3">
							<legend><h4>Finalidad del Tratamiento de Datos</h4></legend>
							<p>Sus datos personales podrán ser utilizados para las siguientes finalidades relacionadas con su integración a nuestra comunidad de médicos:</p>
								<ul>
									<li>Identificación.</li>
									<li>Investigaciones de mercado y promoción de productos</li>
									<li>Para informarle de nuestros avances, promociones, eventos y en general, mantener contacto con usted.</li>
									<li>Hacer consultas, investigaciones y revisiones en relación a sus quejas o reclamaciones.</li>
									<li>Para fines estadísticos.</li>
									<li>Intercambio de información científica y educativa.</li>
									<li>Participación en programas médicos, de conocimiento de enfermedades y estudios clínicos.</li>
									<li>Reportes a las autoridades sanitarias relativas a posibles experiencias adveras a medicamentos.</li>
								</ul>
							<p>Si usted no desea que PROBIOMED® trate su información para fines publicitarios, promocionales y de investigaciones de mercado podrá revocar su 
							consentimiento comunicándose con nosotros al correo electrónico: <b>privacidad@probiomed.com.mx</b></p>
						</fieldset>
						<fieldset class="aviso4">
							<legend><h4>Transferencia  y Remisión de Datos</h4></legend>
							<p>Su información personal no será transferida sin su previo consentimiento. </p>
							<p>Los datos personales recabados por cualquier medio electrónico o impreso para efectos de sus procesos de producción, comercialización, investigación 
							de mercados y/o de promoción de sus productos, así como los derivados de relaciones contractuales y/o laborales, se tratarán en los términos de la Ley, 
							para su almacenamiento, identificación, administración, análisis, operación y/o divulgación. Dichos datos pueden ser remitidos a empresas subsidiarias y 
							terceros con relaciones contractuales con la compañía. Para ello, en el contrato correspondiente PROBIOMED® incluirá una cláusula de que dichas empresas 
							subsidiarias y terceros otorgan el nivel de protección de datos personales requerido por la Ley.</p>
							<p>Asimismo, para cumplir la(s) finalidad(es) anteriormente descrita(s) u otras aquellas exigidas legalmente o por las autoridades competentes, 
							PROBIOMED® podrá transferir a sus empresas subsidiarias y a las autoridades competentes cierta información recopilada de usted.
							En todo momento, PROBIOMED® salvaguardará la confidencialidad de los datos y el procesamiento de estos de tal manera que su privacidad esté 
							protegida en términos de la Ley, garantizando el cumplimiento del presente aviso por la empresa y por aquellos terceros con quienes mantenga 
							una relación jurídica para la adecuada prestación de sus servicios.</p>
						</fieldset>
						<fieldset class="aviso5">
							<legend><h4>Ejercicio de derechos ARCO</h4></legend>
							<p>Con apego a lo estipulado por la Ley, usted, o su representante legal en su caso, podrá ejercer los derechos de acceso, rectificación, 
							cancelación y oposición (en lo sucesivo Derechos Arco) a través de la solicitud correspondiente, a la dirección de correo electrónica 
							<b>privacidad@probiomed.com.mx</b>donde personal especializado atenderá en tiempo y forma su solicitud.</p>
						</fieldset>
						<fieldset class="aviso6">
							<legend><h4>Revocación del Consentimiento y limitación de uso o divulgación</h4></legend>
							<p>En todo momento, usted podrá revocar el consentimiento que nos ha otorgado para el tratamiento de sus datos personales, siempre y 
							cuando no sea necesario para cumplir obligaciones derivadas de nuestra relación jurídica, a fin de que dejemos de hacer uso o divulgación 
							de los mismos. Igualmente usted  podrá limitar el uso o divulgación de sus datos personales. Para ello, es necesario que presente su petición
						en la siguiente dirección electrónica: <b>privacidad@probiomed.com.mx.</b></p>
						</fieldset>
						<fieldset class="aviso7">
							<legend><h4>Cambios a la Política de Privacidad</h4></legend>
							<p>Cualquier cambio o modificación al presente aviso podrá efectuarse por esta empresa en cualquier momento y se dará a conocer a través de su 
							portal <b>www.probiomed.com.mx</b> y/o a través de medios impresos de circulación nacional.</p>
						</fieldset>
						<fieldset class="aviso8">
							<legend><h4>Comité de Privacidad</h4></legend>
							<p>El Comité de Privacidad de Datos le proporcionará la atención necesaria para el ejercicio de sus derechos ARCO y además, velará por la 
							protección de sus datos personales al interior de la organización.</p>
							<ul>
								<li><b>Dirección:</b> San Esteban No. 88 Col. Santo Tomás, Del. Azcapotzalco, México DF, C.P. 02020.</li>
								<li><b>Correo electrónico:</b> privacidad@probiomed.com.mx</li>
								<li><b>Teléfono:</b> 11 66 20 00</li>
								<li><b>Horario:</b> 9:00 a 18:00 horas</li>
							</ul>
						</fieldset>
					</aside>
				</article>
				<a class="close" href="#close"></a>
	 	</section>	

	</section>
	<footer>
        <aside id="aviso">
            <a href="#aviso_priv" id="aviso" class="cambio">Aviso de Privacidad </a>
            <a href="registrosssa-ipps.php" id="registros" class="cambio">Registros SSA e IPP'S </a>
        </aside>
		<aside id="compatibilidad">Compatibilidad con: <img src="img/navegadores.png"><img class="marca" src="img/probiomed.png"> </aside> 
		<aside id="derechos">® 2013 Todos los derechos reservados, Probiomed ®, SA de CV</aside>
	</footer>
</body>
</html>