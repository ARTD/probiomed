<!DOCTYPE hmtl>
<?php 

require ("funciones.php");

$nueva=0;
$registrar=0;

if(isset($_POST['nueva']))
{    
    $nueva = 1;
    $registrar=1;
    $error = nueva($_GET['v'], $_POST['pass'], $_POST['pass1']);
}
	
?>
<html lang="es-mx">
<head>
  <meta charset= "utf-8" />
  <title>Probiomed</title>
  <meta name="description" content="Sitio exclusivo para Probiomed-Recuperación de contraseña" />
  <link type="image/x-icon" href="img/favicon.ico" rel="icon"/>
  <link rel="stylesheet" href="css/estilos.css" />
  <link rel="sitemap" type="application/xml" title="Sitemap" href="sitemap.xml" />
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
  <script>
    window.jQuery || document.write("<script src='js/jquery.min.js'><\/script>");
  </script>
  <script type="text/javascript" src="js/efectos.js"></script>
</head>
<body>
    <header id="cabecera">
       <img src="img/logo_pr.png" />
    </header>
      <a id="regresar" href="contenido.php"><img id="pastilla" src="img/PASTILLA regresar.png"></a>
    <section class="recuperacion">
      <form name="login" method="post" action="">
        <h2>Introduce tu nueva contraseña</h2>
        <article>
          <label>Contraseña:</label>
          <input type="password" id="pass" name="pass" placeholder="Escribe la nueva contraseña" required/>
        </article>
        <article>
          <label>Confirmar Contraseña:</label>
          <input type="password" id="pass1" name="pass1" placeholder="Escribe nuevamente la contraseña" required/>
        </article>
          <input class="btn" align="center" type="submit" name="nueva" id="nueva" value="Enviar" />
    <?php  
        switch ($error) {
            case -1://error
                echo '<aside id="errores" class="error5"><p>No coinciden las contraseñas.</p></aside>';
                break;
            case -2://error
                echo '<aside id="errores" class="error5"><p>La contraseña no se ha modificado.Vuelve a intentarlo.</p></aside>';
                break;
            case -3://advetencia
                echo '<aside id="errores" class="error6"><p>La contraseña debe tener al menos 4 caracteres.</p></aside>';
                break;//correcto   
            default:
                if($registrar) echo '<aside id="errores" class="error7"><p>Se ha modificado la contraseña correctamente.</p></aside>';
                break;
          }
    ?>
    </section>
<a href="#" class="vent_emergente" id="aviso_priv"></a>
    <section class="popup2">
        <h2>Política de Privacidad</h2>
        <p>La confidencialidad y debida protección de la información personal confiada a PROBIOMED es de máxima importancia. 
          PROBIOMED está comprometido a manejar sus datos personales de manera responsable y con apego a lo previsto por la Ley Federal 
          de Protección de Datos Personales en Posesión de los Particulares (en adelante la “Ley”) y demás normatividad aplicable.</p>
        <p>Para PROBIOMED resulta necesaria la recopilación de ciertos datos personales para llevar a cabo las actividades intrínsecas a su giro 
          comercial y mercantil. PROBIOMED tiene la obligación legal y social de cumplir con las medidas, legales y de seguridad suficientes para proteger 
          aquellos datos personales que haya recabado para las finalidades que en la presente política de privacidad serán descritas.</p>
        <p>Todo lo anterior se realiza con el objetivo de que usted tenga pleno control y decisión sobre sus datos personales. Por ello, le recomendamos que 
          lea atentamente la siguiente información.</p>
        <p>Usted tendrá disponible en todo momento esta política de privacidad en nuestro sitio web <b>www.probiomed.com.mx.</b></p>
        <article class="aviso">     
          <aside class="aviso_listado">
            <ul>
              <li class="1">Datos del responsable</li>
              <li class="2">Datos Personales recabados y datos sensibles</li>
              <li class="3">Finalidad del Tratamiento de Datos</li>
              <li class="4">Transferencia  y remisión de datos</li>
              <li class="5">Ejercicio de derechos ARCO</li>
              <li class="6">Revocación del Consentimiento y limitación de uso o divulgación</li>
              <li class="7">Cambios a la Política de Privacidad</li>
              <li class="8">Comité de Privacidad</li>
            </ul>
          </aside>
          <aside class="aviso_cont">
            <fieldset class="aviso1">
              <legend><h4>Datos del responsable</h4></legend>
              <p>PROBIOMED es una sociedad constituida de conformidad con las leyes de México con domicilio en Av. Ejercito Nacional No. 499, Col. Granada, 
                delegación Miguel Hidalgo, México, Distrito Federal.</p>
            </fieldset>
            <fieldset class="aviso2">
              <legend><h4>Datos Personales recabados y datos sensibles</h4></legend>
              <p>Como parte de nuestra comunidad de médicos, podrá ser recabada y tratada cierta información susceptible de identificación personal sobre usted.
              Entre dicha información se podrá incluir de manera enunciativa, más no limitativa, la siguiente:</p>
              <li> Datos de identificación: nombre completo, teléfono particular, celular y/o de trabajo y fecha de nacimiento.</li>
              <li> Datos profesionales: cédula profesional, y especialidad principal.</li>
              <p>Asimismo le informamos que para cumplir con las finalidades de nuestra relación no es necesario tratar ningún tipo de dato sensible por
                lo que no serán recabados.</p>
              <p>Para las finalidades señaladas en el presente aviso, podemos recabar sus datos personales de distintas formas: cuando usted nos los
              proporciona directamente; cuando visita nuestro sitio de Internet o utiliza nuestros servicios en línea, y cuando obtenemos información 
              a través de otras fuentes que están permitidas por la Ley.</p>
            </fieldset>
            <fieldset class="aviso3">
              <legend><h4>Finalidad del Tratamiento de Datos</h4></legend>
              <p>Sus datos personales podrán ser utilizados para las siguientes finalidades relacionadas con su integración a nuestra comunidad de médicos:</p>
                <ul>
                  <li>Identificación.</li>
                  <li>Investigaciones de mercado y promoción de productos</li>
                  <li>Para informarle de nuestros avances, promociones, eventos y en general, mantener contacto con usted.</li>
                  <li>Hacer consultas, investigaciones y revisiones en relación a sus quejas o reclamaciones.</li>
                  <li>Para fines estadísticos.</li>
                  <li>Intercambio de información científica y educativa.</li>
                  <li>Participación en programas médicos, de conocimiento de enfermedades y estudios clínicos.</li>
                  <li>Reportes a las autoridades sanitarias relativas a posibles experiencias adveras a medicamentos.</li>
                </ul>
              <p>Si usted no desea que PROBIOMED trate su información para fines publicitarios, promocionales y de investigaciones de mercado podrá revocar su 
              consentimiento comunicándose con nosotros al correo electrónico: <b>privacidad@probiomed.com.mx</b></p>
            </fieldset>
            <fieldset class="aviso4">
              <legend><h4>Transferencia  y remisión de datos</h4></legend>
              <p>Su información personal no será transferida sin su previo consentimiento. </p>
              <p>Los datos personales recabados por cualquier medio electrónico o impreso para efectos de sus procesos de producción, comercialización, investigación 
              de mercados y/o de promoción de sus productos, así como los derivados de relaciones contractuales y/o laborales, se tratarán en los términos de la Ley, 
              para su almacenamiento, identificación, administración, análisis, operación y/o divulgación. Dichos datos pueden ser remitidos a empresas subsidiarias y 
              terceros con relaciones contractuales con la compañía. Para ello, en el contrato correspondiente PROBIOMED incluirá una cláusula de que dichas empresas 
              subsidiarias y terceros otorgan el nivel de protección de datos personales requerido por la Ley.</p>
              <p>Asimismo, para cumplir la(s) finalidad(es) anteriormente descrita(s) u otras aquellas exigidas legalmente o por las autoridades competentes, 
              PROBIOMED podrá transferir a sus empresas subsidiarias y a las autoridades competentes cierta información recopilada de usted.
              En todo momento, PROBIOMED salvaguardará la confidencialidad de los datos y el procesamiento de estos de tal manera que su privacidad esté 
              protegida en términos de la Ley, garantizando el cumplimiento del presente aviso por la empresa y por aquellos terceros con quienes mantenga 
              una relación jurídica para la adecuada prestación de sus servicios.</p>
            </fieldset>
            <fieldset class="aviso5">
              <legend><h4>Ejercicio de derechos ARCO</h4></legend>
              <p>Con apego a lo estipulado por la Ley, usted, o su representante legal en su caso, podrá ejercer los derechos de acceso, rectificación, 
              cancelación y oposición (en lo sucesivo Derechos Arco) a través de la solicitud correspondiente, a la dirección de correo electrónica 
              <b>privacidad@probiomed.com.mx</b>donde personal especializado atenderá en tiempo y forma su solicitud.</p>
            </fieldset>
            <fieldset class="aviso6">
              <legend><h4>Revocación del Consentimiento y limitación de uso o divulgación</h4></legend>
              <p>En todo momento, usted podrá revocar el consentimiento que nos ha otorgado para el tratamiento de sus datos personales, siempre y 
              cuando no sea necesario para cumplir obligaciones derivadas de nuestra relación jurídica, a fin de que dejemos de hacer uso o divulgación 
              de los mismos. Igualmente usted  podrá limitar el uso o divulgación de sus datos personales. Para ello, es necesario que presente su petición
            en la siguiente dirección electrónica: <b>privacidad@probiomed.com.mx.</b></p>
            </fieldset>
            <fieldset class="aviso7">
              <legend><h4>Cambios a la Política de Privacidad</h4></legend>
              <p>Cualquier cambio o modificación al presente aviso podrá efectuarse por esta empresa en cualquier momento y se dará a conocer a través de su 
              portal <b>www.probiomed.com.mx</b> y/o a través de medios impresos de circulación nacional.</p>
            </fieldset>
            <fieldset class="aviso8">
              <legend><h4>Comité de Privacidad</h4></legend>
              <p>El Comité de Privacidad de Datos le proporcionará la atención necesaria para el ejercicio de sus derechos ARCO y además, velará por la 
              protección de sus datos personales al interior de la organización.</p>
              <ul>
                <li><b>Dirección:</b> Av. Ejercito Nacional No. 499, Col. Granada, delegación Miguel Hidalgo, México, Distrito Federal.</li>
                <li><b>Correo electrónico:</b> privacidad@probiomed.com.mx</li>
                <li><b>Teléfono:</b> 11 66 20 00</li>
                <li><b>Horario:</b> 9:00 a 18:00 horas</li>
              </ul>
            </fieldset>
          </aside>
        </article>
        <a class="close" href="#close"></a>
    </section>  
  <footer>
    <a href="#aviso_priv" id="aviso" class="cambio">Aviso de Privacidad</a>
    <aside id="derechos">2013 Todos los derechos reservados, Probiomed, SA de CV</aside>
    <aside id="compatibilidad">Compatibilidad con: <img src="img/navegadores.png"> </aside>
    <img class="marca" src="img/probiomed.png">
  </footer>
</body>
</html>