<?php
require('mysql_table.php');
include('conexion.php');


class PDF extends PDF_MySQL_Table {
	function Header() {
		//Title
		$this->SetFont('Arial','',18);
		$this->Image('img/logo_pr.png',20,8,40);
		$date = date("d/m/Y");
		$this->SetX(135);
		$this->Cell(70,20,'PROBIOMED Rewards reporte de ganadores farmaceuticos '.$date,0,0,'C');
		// Salto de línea
		$this->Ln(30);
		//Ensure table header is output
		parent::Header();
	}
	function Footer() {
		// Posición: a 1,5 cm del final
	    $this->SetY(-15);
	    // Arial italic 8
	    $this->SetFont('Arial','I',8);
	    // Número de página
	    $this->Cell(0,10,'Pagina '.$this->PageNo(),0,0,'C');
	}
}


$pdf=new PDF();
$pdf->AddPage('L');
$pdf->AddCol('nombre',50,'Nombre','C');
$pdf->AddCol('apellido_pa',50,'Apellido Paterno','C');
$pdf->AddCol('apellido_ma',50,'Apellido Materno','C');
$pdf->AddCol('usuario',70,'Usuario','C');
$pdf->AddCol('puntos',30,'Puntos','C');
$prop=array('HeaderColor'=>array(140,132,254),
			'color1'=>array(220,220,220),
			'color2'=>array(255,255,255),
			'padding'=>0);
$pdf->Table("SELECT * FROM ganadoresfarma",$prop);
$pdf->Output();
?>
