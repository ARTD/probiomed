#Paso numero 
#Ejecutar este pedazo de script

#Este pedazo de código se encarga de crear una funcion para los representantes médicos, la cual toma el nomnbre,
#apellido paterno, apellido materno, usuario y puntaje de los medicos a su cargo
DROP PROCEDURE IF EXISTS pr_Representante;
DELIMITER $$
CREATE PROCEDURE pr_Representante (IN representa VARCHAR(45))
BEGIN
	SELECT d.nombre, d.apellido_pa, d.apellido_ma, u.usuario, u.puntos
	FROM datos d
	JOIN usuarios u
	ON d.id_datos = u.id_usuarios
	WHERE d.representante = representa;
END$$
DELIMITER ;

-------------------------------------------------------------------------------------------------------------------------
#Paso numero 
#Ejecutar este pedazo de script

#Este procedimiento sirve para guardar un historial de los puntos de los usuarios
#En caso de que ya exista no tiene porque volver a ejecutarse
DROP PROCEDURE IF EXISTS pr_HistorialPuntos;
CREATE PROCEDURE pr_HistorialPuntos()
BEGIN
  SET @lu=0;

  SELECT @lu := COUNT(*) FROM usuarios;

  WHILE @lu > 0 DO
    UPDATE usuarios SET historial_puntos = historial_puntos+puntos WHERE id_usuarios = @lu;
    SET @lu = @lu - 1;
  END WHILE;
END;

-------------------------------------------------------------------------------------------------------------------------
#Paso numero 
#Ejecutar este pedazo de script

#Este procedimiento sirve para actualizar los tres primeros lugares en la table provi
#En caso de que ya exista no tiene porque volver a ejecutarse
DROP PROCEDURE IF EXISTS pr_Lugares;
CREATE PROCEDURE pr_Lugares()
BEGIN
  DECLARE v1 INT DEFAULT 10;
  DECLARE lu INT DEFAULT 10;

  WHILE v1 > 0 DO
    UPDATE provi SET lugar=lu WHERE id_provi = lu;
    SET lu = lu - 1;
    SET v1 = v1 - 1;
  END WHILE;
END;

-------------------------------------------------------------------------------------------------------------------------
#Paso numero 
#Ejecutar este pedazo de script

#Esta funcion se ejecuta mensualmente y realiza las operaciones necesarias para meter los ganadores mensuales a la tabla ganadores,
#y resetear los puntos de los usuarios a 0 para que tengan oportunidad de ganar premios
DROP EVENT IF EXISTS sc_Mes;
DELIMITER $$
CREATE EVENT sc_Mes
ON SCHEDULE EVERY '1' MONTH
STARTS '2013-04-29 00:00:00'
DO 
BEGIN
	#Inserta los ganadores medicos mensuales en una tabla provisional
	INSERT INTO provi (nombre, apellido_pa, apellido_ma, especialidad, usuario, puntos, mes) SELECT d.nombre, d.apellido_pa, d.apellido_ma, e.especialidad, u.usuario, u.puntos, MONTHNAME(CURRENT_DATE()) FROM datos d JOIN usuarios u JOIN especialidad e WHERE d.id_datos = u.id_datos AND u.id_especialidad = e.id_especialidad ORDER BY u.puntos DESC LIMIT 10;
	#Llama a una función que previamente fue creada llamada pr_Lugares();
	#Esta funcion se encarga de posicionar los lugares correspondientes a los ganadores
	CALL pr_Lugares();
	#Inserta los datos completos de los ganadores medicos en la tabla final de ganadoresmedico
	INSERT INTO ganadoresmedico (nombre, apellido_pa, apellido_ma, especialidad, usuario, puntos, lugar,mes) SELECT nombre, apellido_pa, apellido_ma,especialidad,usuario,puntos,lugar,mes FROM provi;
	#Se limpian los datos de la tabla provi para futuras operaciones
	TRUNCATE TABLE provi;
	#Inserta los ganadores farmaceuticos mensuales en una tabla provisional
	INSERT INTO provi (nombre, apellido_pa, apellido_ma, usuario, puntos, mes) SELECT d.nombre, d.apellido_pa, d.apellido_ma, u.usuario, u.puntos, MONTHNAME(CURRENT_DATE()) FROM datos d JOIN usuarios u WHERE d.id_datos = u.id_datos AND u.id_tipo = 2 ORDER BY u.puntos DESC LIMIT 10;
	#Llama a una función que previamente fue creada llamada pr_Lugares();
	#Esta funcion se encarga de posicionar los lugares correspondientes a los ganadores
	CALL pr_Lugares();
	#Inserta los datos completos de los ganadores farmaceuticos en la tabla final de ganadoresfarma
	INSERT INTO ganadoresfarma (nombre, apellido_pa, apellido_ma, usuario, puntos, lugar,mes) SELECT nombre, apellido_pa, apellido_ma,usuario,puntos,lugar,mes FROM provi;
	#Llama a una función que previamente fue creada llamada pr_HistorialPuntos();
	#Esta funcion se encarga de guardar los puntos de los usuarios en un historial
	CALL pr_HistorialPuntos();
	#Se limpian los datos de la tabla provi para futuras operaciones
	TRUNCATE TABLE provi;
	#Resetea los puntos de todos los usuarios a 0 para que vuelvan a tener la oportunidad de ganar premios
	UPDATE usuarios SET puntos = 0;
	#Se limpia la tabla de comentarios
	TRUNCATE TABLE comentarios;
END$$
DELIMITER ;

-------------------------------------------------------------------------------------------------------------------------
#Paso numero 
#Ejecutar este pedazo de script

#Esta funcion sirve para habilitar los eventos programados por calendario
#Se tiene que ejecutar despues de haber ejecutado el script anterior
SET GLOBAL event_scheduler = 1;