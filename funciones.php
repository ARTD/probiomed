<?php
session_start();

$salt = "|#€7`¬23ads4ook12";
$saltCookie = "|@#57e+ç´|@#d";



/**
 * Comprueba que exista una sesion o una cookie en la página de login
 *
 * 
 */
function seguridadIndex()
{
    
    if (isset($_SESSION['usuarios']))
    {
        
        header("Location: contenido.php");
        exit();
    }
    else if( isset($_COOKIE['identificado']))
    {     
        $cookie = limpiar($_COOKIE['identificado']);
        $idusuario = comprobarCookie($cookie);
        if(!$idusuario)
        {
            header("Location: contenido.php");
            exit();
        }
    }
}


/**
 * Comprueba que exista una sesion o una cookie, sino redirige al login
 *
 * @return int estado
 */
function seguridad(){

  /*  $conexion=mysql_connect("212.1.214.148","deepl295_pro","probiomed",false);
    $bd = mysql_select_db("deepl295_probiomed",$conexion);
    mysql_query("SET NAMES 'utf8'"); */

    include "conexion.php";

    if (isset($_SESSION['usuarios']))
    {
        return;
    }
    else if( isset($_COOKIE['identificado']))
    {     
        $cookie = limpiar($_COOKIE['identificado']);
        $idusuario = comprobarCookie($cookie);
        if(!$idusuario)
        {
            /* echo "<script language='javascript'> document.location.href='index.php' </script>";
            exit(); */
        }
    } 
    else 
    {
       echo "<script language='javascript'> document.location.href='index.php' </script>";
        exit(); 
    }

}

/**
 * Comprueba que la cookie sea validad en nuestra BD
 *
 * @param string $cookie
 * @return int idUsuario
 */
function comprobarCookie($cookie)
{
   /* $conexion=mysql_connect("212.1.214.148","deepl295_pro","probiomed",false);
    $bd = mysql_select_db("deepl295_probiomed",$conexion);
    mysql_query("SET NAMES 'utf8'"); */

    include "conexion.php";
    // $sql = "select id_usuarios from usuarios where cookie=' ".mysql_escape_string($cookie)."' and validez>'".date("Y-m-d h:i:s")."'";
    $sql = "select id_usuarios from usuarios where cookie=' ".$cookie."' and validez>'".date("Y-m-d h:i:s")."'";
    $result = mysql_query($sql,$conexion);
    
    if(!$result || mysql_affected_rows()<1) return false;
    else
    {
        $row = mysql_fetch_array($result);
        $_SESSION['usuarios']=$row['id_usuarios'];
        return $row['s'];
    }

}


/**
 * Registra un usuario con seguridad
 *
 * @global string $salt
 * @param string $user
 * @param string $pass
 * @return int 
 */

function comentario($u,$coment)

{
    include "conexion.php";
    $sql = "insert into comentarios(usuario,comentario,fecha) values ('".$u."','".$coment."','".date('Y-m-d')."')";
    $result = mysql_query($sql,$conexion);
    if($result) return 1;
    else return -2;
}

function registrarUsuario($nombre,$apellidop,$apellidom,$email,$tipo,$representante,$cedula,$user,$pass)
{
   
    if(strlen($user)<4 || strlen($pass)<4) return -3;
    
    global $salt;
    $pass = sha1($salt.md5($pass));

    include "conexion.php";
    
    $sql1 = "select id_usuarios from usuarios where UPPER(usuario)='".strtoupper($user)."'";
    $result1 = mysql_query($sql1,$conexion);
    if(mysql_affected_rows()>0) return -2; //user repetido
    
     $sql3 = "select A.correo from datos A, usuarios B where B.usuario='".strtoupper($user)."' and B.id_datos = A.id_datos;";
    $result3 = mysql_query($sql3,$conexion);
    if(mysql_affected_rows()>0) return -2; //user repetido 
    
    $sql = "insert into datos (nombre,apellido_pa,apellido_ma,correo,representante,cedula) values ('".$nombre."','".$apellidop."','".$apellidom."','".$email."','".$representante."','".$cedula."')";

    $result = mysql_query($sql,$conexion);

    $ultimo_id =  mysql_insert_id($conexion);

    $sql1 = "insert into usuarios (usuario,password,puntos,id_datos,id_tipo) values ('".$user."','".$pass."','0','".$ultimo_id."','".$tipo."')";
    
    $result1 = mysql_query($sql1,$conexion);
    
    if($result and $result1) return 1; //registro correcto
    else return -2; //error
}

function nueva($v,$pass,$pass1)
{
    if(strlen($pass)<4 || strlen($pass1)<4) return -3;

    include "conexion.php";
    $sql = "select cookie from usuarios where nueva ='".strtoupper($v)."'";
    $result = mysql_query($sql,$conexion);

    if(mysql_affected_rows()>0)
    {    

       // if(strcmp($pass, $pass1) != 0) return -1;

        if(strcmp($pass, $pass1) != 0) return -1;
            
        global $salt;
        $pass = sha1($salt.md5($pass));
      
        $sql2 = "update usuarios set password='".$pass."' where `nueva`='".$v."'";
        $result2 = mysql_query($sql2,$conexion);

        $nueva = sha1($saltCookie.md5($idUsuario.date("Y-d-m h:i:s")));
        $sql3 = "update usuarios set nueva='".$cookie."',validez=DATE_ADD(now(),INTERVAL 6 MINUTE) where `nueva`='".$v."'";
        $result3 = mysql_query($sql3,$conexion);

        if($result2 and $result3) return 1;

        else return -2;

    }   
    
}

function passolvido($email)
{
    include "conexion.php";
    $sql = "select correo from datos where correo ='".strtoupper($email)."'";
    $result = mysql_query($sql,$conexion);

    if(mysql_affected_rows()>0)
    {
        $sql = "select A.password from usuarios A, datos B where B.correo='".$email."' and A.id_datos = B.id_datos";
        $result1 = mysql_query($sql,$conexion);
        $row1 = mysql_fetch_row($result1);
        $pass = $row1[0];

        global $saltCookie;

        $cookie = sha1($saltCookie.md5($idUsuario.date("Y-d-m h:i:s")));
        $sql2 = "update usuarios set nueva='".$cookie."',validez=DATE_ADD(now(),INTERVAL 6 MINUTE) where `password`='".$pass."'";
        $result = mysql_query($sql2,$conexion);


        require("class.phpmailer.php");
        $mail = new PHPMailer();
        $mail->From = "correo@peermx.net"; // Mail de origen
        $mail->FromName = "Probiomed"; // Nombre del que envia
        $mail->AddAddress($email); // Mail destino, podemos agregar muchas direcciones
        $mail->AddReplyTo($mailfrom); // Mail de respuesta

        $mail->WordWrap = 50; // Largo de las lineass
        $mail->IsHTML(true); // Podemos incluir tags html
        $mail->Subject  =  "Recordatorio de Contraseña:";
        $mail->Body     =  "<p>Has solicitado una nueva contraseña</p>
                            <p>Para cambiarla da click en el siguiente enlace</p>
                            <p>http://".$_SERVER['HTTP_HOST']."/nueva.php?v=".$cookie."</p>
                            <p>Si usted no la solicito o fue un error solo ignore el mensaje</p>
                            <p>Probiomed</p>";
        $mail->AltBody  =  strip_tags($mail->Body); // Este es el contenido alternativo sin html

        $mail->Host     = "localhost";

        if ($mail->Send())
        
        echo "Enviado";

        else

        echo "Error en el envio de mail";
    }

    else return -5;

}

/**
 * Comprueba y el user y pass son correcto. En caso de querer ser recordado en el pc, crea la cookie
 *
 * @global string $salt
 * @global string $saltCookie
 * @param string $user
 * @param string $pass
 * @param bool $recordarme
 * @return int estado 
 */
function login ($user,$pass,$recordarme)
{
  /*  $user = mysql_escape_string($user);
    $pass = mysql_escape_string($pass); */
    
    if(strlen($user)<4 || strlen($pass)<4) return -3;
    
    global $salt;
    $pass = sha1($salt.md5($pass));
    
    /* $conexion=mysql_connect("212.1.214.148","deepl295_pro","probiomed",false);
    $bd = mysql_select_db("deepl295_probiomed",$conexion); 
    mysql_query("SET NAMES 'utf8'"); */
    
    include "conexion.php";

    $sql = "select * from usuarios where UPPER(usuario)='".strtoupper($user)."' and password='".$pass."'";
    $result = mysql_query($sql,$conexion);

  

    if(mysql_affected_rows()<=0 || !$result) return -1; //user repetido
    
    $row = mysql_fetch_array($result);
    $idUsuario = $row['id_usuarios'];
    $_SESSION['usuarios']=$idUsuario;
    
    if($recordarme){
        global $saltCookie;

        $cookie = sha1($saltCookie.md5($idUsuario.date("Y-d-m h:i:s")));

        $sql2 = "update usuarios set cookie='".$cookie."',validez=DATE_ADD(now(),INTERVAL 6 MINUTE) where `id_usuarios`='".$idUsuario."'";
        $result2 = mysql_query($sql2,$conexion);

        setCookie("identificado",$cookie,time()+604800,'/'); 
    }
    $_SESSION['usuarios']=$idUsuario;

    $sql1 = "select * from usuarios where UPPER(usuario)='".strtoupper($user)."' and password='".$pass."'";
    $result1 = mysql_query($sql1,$conexion);
    $row1 = mysql_fetch_row($result1);

    $tipousuario = $row1[7];
    $_SESSION['tipousuario']=$tipousuario;

    
    $sql3 = "select B.representante from usuarios A, datos B where A.id_usuarios= 1 and A.id_datos = B.id_datos";
    
    $result3 = mysql_query($sql3,$conexion);
    $row3 = mysql_fetch_row($result3);

    $representante = $row3[0];
    $_SESSION['representante']=$representante; 

    return true;
   
}



function destruirCookie($cookie)
{
    if(!isset($_SESSION['usuarios'])) return;
    else $idusuario = $_SESSION['usuarios'];
    
   /* $conexion=mysql_connect("212.1.214.148","deepl295_pro","probiomed",false);
    $bd = mysql_select_db("deepl295_probiomed",$conexion);
    mysql_query("SET NAMES 'utf8'"); */

    include "conexion.php";
    
    $sql = "update usuarios set validez=DATE_SUB(now(),INTERVAL 6 MINUTE) where `id_usuarios`='".$idusuario."'";
    $result = mysql_query($sql2,$conexion);
    if(mysql_affected_rows()>0) return true; //cookie puesta invalida
    else return false;
    
    
}

/**
 *
 * @param string $valor
 * @return string string limpiado de fallos de seguridad
 */
function limpiar($valor){
    $valor = strip_tags($valor);
    $valor = stripslashes($valor);
    $valor = htmlentities($valor);
    return $valor;
}

?>